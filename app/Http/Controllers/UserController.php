<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\User;
use App\Rules\MatchOldPassword;
use Illuminate\Support\Facades\Hash;
use auth;

class UserController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('user');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(request $request)
    {
        $data = new User();
        $data->fullname = $request->fullname;
        $data->email = $request->email;
        $data->userlevel = $request->role;
        $data->kodegudang = $request->kodegudang;
        $data->password = Hash::make($request->password);
        if($data->save()){
            echo "Data telah disimpan.";
        } else {
            echo "Data gagal disimpan.";
        }
        
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(request $request)
    {
        $data = User::find($request->id);
        return json_encode($data);
    }

    public function data()
    {
        $data = User::selectRaw("id, fullname, email, userlevel, kodegudang, date_format(users.created_at,'%d-%m-%Y %H:%i') as date")->orderBy('id','desc')->get();
        return json_encode(['data'=>$data]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(request $request)
    {
        $data = User::find($request->id);
        $data->fullname = $request->fullname;
        $data->email = $request->email;
        $data->userlevel = $request->role;
        $data->kodegudang = $request->kodegudang;
        if($data->save()){
            echo "Data telah disimpan.";
        } else {
            echo "Data gagal disimpan.";
        }
    }

    public function editpassword(request $request)
    {
        $data = User::find($request->id);
        $data->password = Hash::make($request->current_password);
        if($data->save()){
            echo "Data telah disimpan.";
        } else {
            echo "Data gagal disimpan.";
        }
    }

    public function destroy(request $request)
    {
        $data = User::find($request->id);
        if($data->delete()){
            echo "Data telah dihapus.";
        } else {
            echo "Data gagal dihapus.";
        }
    }
}
