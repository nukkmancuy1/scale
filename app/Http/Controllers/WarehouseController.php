<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Warehouse;
use Auth;

class WarehouseController extends Controller
{
    public function index()
    {
        $user = Auth::user()->name;
        return view('warehouse', compact('user'));
    }

    public function create(request $request)
    {
        $data = new Warehouse();
        $data->kodegudang = $request->kode;
        $data->namagudang = $request->nama;
        $data->created_by = Auth::user()->id;
        if($data->save()){
            return json_encode('Data telah disimpan.');
        } else {
            return json_encode('Data gagal disimpan.');
        }
    }

    public function detail(request $request)
    {
        $Warehouse = Warehouse::find($request->id);
        return json_encode($Warehouse);
    }

    public function data()
    {
        $Warehouse = Warehouse::selectRaw("warehouse.id, warehouse.kodegudang, warehouse.namagudang, users.fullname, date_format(warehouse.created_at,'%d-%m-%Y %H:%i') as date")->leftJoin('users','warehouse.created_by','=','users.id')->orderBy('warehouse.id','desc')->get();
        return json_encode(['data'=>$Warehouse]);
    }

    public function edit(request $request)
    {
        $Warehouse = Warehouse::find($request->id);
        $Warehouse->kodegudang = $request->kode;
        $Warehouse->namagudang = $request->nama;
        $Warehouse->created_by = Auth::user()->id;
        if($Warehouse->save()){
            return json_encode('Data telah disimpan.');
        } else {
            return json_encode('Data gagal disimpan.');
        }
    }

    public function destroy(request $request)
    {
        $Warehouse = Warehouse::find($request->id);
        if($Warehouse->delete())
            {
                return json_encode('Data telah dihapus');
            } else {
                return json_encode('Data gagal dihapus');
            }
    }
}
