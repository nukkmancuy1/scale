<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Item;
use Auth;

class ItemController extends Controller
{
    public function index()
    {
        $user = Auth::user()->name;
        return view('item', compact('user'));
    }

    public function create(request $request)
    {
        $data = new Item();
        $data->kodeitem = $request->kode;
        $data->namaitem = $request->nama;
        $data->sample = $request->sample;
        $data->created_by = Auth::user()->id;
        if($data->save()){
            return json_encode('Data telah disimpan.');
        } else {
            return json_encode('Data gagal disimpan.');
        }
    }

    public function detail(request $request)
    {
        $Item = Item::find($request->id);
        return json_encode($Item);
    }

    public function data()
    {
        $Item = Item::selectRaw("item.id, item.kodeitem, item.namaitem, item.sample, users.fullname, date_format(item.created_at,'%d-%m-%Y %H:%i') as date")->leftJoin('users','item.created_by','=','users.id')->orderBy('item.id','desc')->get();
        return json_encode(['data'=>$Item]);
    }

    public function edit(request $request)
    {
        $data = Item::find($request->id);
        $data->kodeitem = $request->kode;
        $data->namaitem = $request->nama;
        $data->sample = $request->sample;
        $data->created_by = Auth::user()->id;
        if($data->save()){
            return json_encode('Data telah disimpan.');
        } else {
            return json_encode('Data gagal disimpan.');
        }
    }

    public function destroy(request $request)
    {
        $Item = Item::find($request->id);
        if($Item->delete())
            {
                return json_encode('Data telah dihapus');
            } else {
                return json_encode('Data gagal dihapus');
            }
    }
}
