<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Hits;
use Auth;
use DB;

class HitsController extends Controller
{
    public function index()
    {
        $user = Auth::user()->name;
        return view('hits', compact('user'));
    }

    public function hitpenerimaan(){
        return view('hitpenerimaan');
    }

    public function datahitpenerimaan(Request $request)
    {
        $awal = isset($request->awal)?$request->awal.' 00:00':date('Y-m-d').' 00:00';
        $akhir = isset($request->akhir)?$request->akhir.' 23:59':date('Y-m-d').' 23:59';
        $data = Hits::selectRaw("
        id, 
        createdfrom, 
        nomor, 
        tanggal, 
        pedagang, 
        notruk, 
        item, 
        colly, 
        qty, 
        status,
        date_format(created_at,'%d-%m-%Y %H:%i') as date
        ")
        ->whereBetween('created_at',[$awal,$akhir])
        ->orderBy('id','desc')
        ->get();
        return json_encode(['data'=>$data]);
    }

    public function create(request $request)
    {
        $data = new Hits();
        $data->createdfrom = $request->createdfrom;
        $data->nomor = $request->nomor;
        $data->tanggal = date_format(date_create($request->tanggal),"Y-m-d");
        $data->penimbang = $request->penimbang;
        $data->notruk = $request->notruk;
        $data->item = $request->item;
        $data->colly = $request->colly;
        $data->qty = $request->qty;
        $data->units = $request->unit;
        $data->serial = $request->serial;
        $data->location = $request->location;
        $data->noitemfulfillment = isset($request->noitemfulfillment)?$request->noitemfulfillment:null;
        $data->pedagang = $request->pedagang;
        $data->idasal = $request->idasal;
        if($data->save()){
            return json_encode('Data telah disimpan.');
        } else {
            return json_encode('Data gagal disimpan.');
        }
    }

    public function detail(request $request)
    {
        $data = Hits::find($request->id);
        $data = Hits::selectRaw('hitspenerimaan.*,notapenerimaan.kodetimbangan')->join('notapenerimaan','hitspenerimaan.idasal','notapenerimaan.id')->find($request->id);
        return json_encode($data);
    }

    public function data(Request $request)
    {
        $awal = isset($request->awal)?$request->awal.' 00:00':date('Y-m-d').' 00:00';
        $akhir = isset($request->akhir)?$request->akhir.' 23:59':date('Y-m-d').' 23:59';
        $Warehouse = Hits::selectRaw("
        notapenerimaan.id, 
        createdfrom, 
        nomor, 
        tanggal, 
        pedagang, 
        notruk, 
        item, 
        bruto, 
        netto, 
        date_format(notapenerimaan.created_at,'%d-%m-%Y %H:%i') as date
        ")
        ->whereBetween('notapenerimaan.created_at',[$awal,$akhir])
        ->orderBy('notapenerimaan.id','desc')
        ->get();
        return json_encode(['data'=>$Warehouse]);
    }

    public function edit(request $request)
    {
        $data = Hits::find($request->id);
        $data->createdfrom = $request->createdfrom;
        $data->nomor = $request->nomor;
        $data->tanggal = date_format(date_create($request->tanggal),"Y-m-d");
        $data->penimbang = $request->penimbang;
        $data->notruk = $request->notruk;
        $data->item = $request->item;
        $data->colly = $request->colly;
        $data->qty = $request->qty;
        $data->units = $request->unit;
        $data->serial = $request->serial;
        $data->location = $request->location;
        $data->noitemfulfillment = isset($request->noitemfulfillment)?$request->noitemfulfillment:null;
        $data->pedagang = $request->pedagang;
        if($data->save()){
            return json_encode('Data telah disimpan.');
        } else {
            return json_encode('Data gagal disimpan.');
        }
    }

    public function destroy(request $request)
    {
        $Warehouse = Hits::find($request->id);
        if($Warehouse->delete())
            {
                return json_encode('Data telah dihapus');
            } else {
                return json_encode('Data gagal dihapus');
            }
    }

    public function replace()
    {
        $r=DB::select("select sum(hp.qty), np.id, np.tanggal, np.pedagang, np.notruk, np.item, if(sum(hp.qty)>0,np.netto - sum(hp.qty), np.netto) as netto, if(sum(hp.colly)>0,np.colly - sum(hp.colly), np.colly) as colly, np.units from notapenerimaan np left outer join hitspenerimaan hp on np.id=hp.idasal and hp.deleted_at is null where np.deleted_at is null group by np.id having netto > 0");
        return json_encode($r);
    }

    public function ubahstatus(request $request)
    {
        $data = Hits::find($request->id);
        $data->status=1;
        if($data->save()){
            return json_encode('Data telah disimpan.');
        } else {
            return json_encode('Data gagal disimpan.');
        }
    }
}
