<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\User;
use App\Rules\MatchOldPassword;
use Illuminate\Support\Facades\Hash;
use auth;
class ProfileController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $user = Auth::user()->fullname;
        $email = Auth::user()->email;
        $id = Auth::user()->id;
        return view('profile', compact('user','email','id'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function changepassword(Request $request)
    {
        $request->validate([
            'current_password' => ['required', new MatchOldPassword],
            'new_password' => ['required'],
            'new_confirm_password' => ['same:new_password'],
        ]);
   
        $user=User::find(auth()->user()->id)->update(['password'=> Hash::make($request->new_password)]);
        if($user){
            return back()->with('success','Password telah disimpan.');
        } else {
            return back()->with('danger','Password gagal disimpan. Mohon periksa kembali.');
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(request $request)
    {
        $data = User::find($request->id);
        $data->delete();
        Auth::logout();
        return redirect('login');
    }

    public function imageUpload()
    {
        return view('imageUpload');
    }

    public function imageUploadPost(Request $request)
    {
        $user = User::find($request->id);
        $user->name = $request->nama;
        $user->email = $request->email;
        $user->save();
        
        /* Store $imageName name in DATABASE from HERE */

        return back()->with('success','Data telah disimpan.');

    }
}
