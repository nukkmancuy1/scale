<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Package;
use Auth;

class PackageController extends Controller
{
    public function index()
    {
        $user = Auth::user()->name;
        return view('package', compact('user'));
    }

    public function create(request $request)
    {
        $data = new Package();
        $data->kodepackage = $request->kode;
        $data->namapackage = $request->nama;
        $data->beratpackage = $request->berat;
        $data->created_by = Auth::user()->id;
        if($data->save()){
            return json_encode('Data telah disimpan.');
        } else {
            return json_encode('Data gagal disimpan.');
        }
    }

    public function detail(request $request)
    {
        $package = Package::find($request->id);
        return json_encode($package);
    }

    public function data()
    {
        $package = Package::selectRaw("package.id, package.kodepackage, package.namapackage, package.beratpackage, users.fullname, date_format(package.created_at,'%d-%m-%Y %H:%i') as date")->leftJoin('users','package.created_by','=','users.id')->orderBy('id','desc')->get();
        return json_encode(['data'=>$package]);
    }

    public function edit(request $request)
    {
        $data = Package::find($request->id);
        $data->kodepackage = $request->kode;
        $data->namapackage = $request->nama;
        $data->beratpackage = $request->berat;
        $data->created_by = Auth::user()->id;
        if($data->save()){
            return json_encode('Data telah disimpan.');
        } else {
            return json_encode('Data gagal disimpan.');
        }
    }

    public function destroy(request $request)
    {
        $data = Package::find($request->id);
        if($data->delete())
            {
                return json_encode('Data telah dihapus');
            } else {
                return json_encode('Data gagal dihapus');
            }
    }
}
