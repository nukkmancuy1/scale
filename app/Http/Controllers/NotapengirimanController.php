<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Notapengiriman;
use App\Models\Datatimbangan;
use App\Models\Timbangan;
use Auth;

class NotapengirimanController extends Controller
{
    public function index()
    {
        $user = Auth::user()->name;
        return view('notapengiriman', compact('user'));
    }

    public function create(request $request)
    {
        $data = new Notapengiriman();
        $data->tanggal = date_format(date_create($request->tanggal),"Y-m-d");
        $data->penimbang = $request->penimbang;
        $data->notruk = $request->notruk;
        $data->item = $request->item;
        $data->colly = $request->colly;
        $data->netto = $request->netto;
        $data->units = $request->unit;
        $data->location = $request->location;
        $data->pedagang = $request->pedagang;
        $data->bruto = $request->bruto;
        $data->tara = $request->tara;
        $pack = $request->kodepackage;
        $jml = $request->jumlahpackage;
        $lpack='';
        for($a=0;$a<count($pack);$a++){
            $lpack=$lpack.$pack[$a].':'.$jml[$a].';';
        }
        $data->kodepackage = $lpack;
        $data->kodetimbangan = $request->kodetimbangan;
        if($data->save()){
            return json_encode('Data telah disimpan.');
        } else {
            return json_encode('Data gagal disimpan.');
        }
    }

    public function detail(request $request)
    {
        $data = Notapengiriman::find($request->id);
        return json_encode($data);
    }

    public function data(Request $request)
    {
        $awal = isset($request->awal)?$request->awal.' 00:00':date('Y-m-d').' 00:00';
        $akhir = isset($request->akhir)?$request->akhir.' 23:59':date('Y-m-d').' 23:59';
        $Warehouse = Notapengiriman::selectRaw("
        notapengiriman.id, 
        tanggal, 
        pedagang, 
        notruk, 
        item, 
        bruto, 
        netto, 
        date_format(notapengiriman.created_at,'%d-%m-%Y %H:%i') as date
        ")
        ->whereBetween('notapengiriman.created_at',[$awal,$akhir])
        ->orderBy('notapengiriman.id','desc')
        ->get();
        return json_encode(['data'=>$Warehouse]);
    }

    public function edit(request $request)
    {
        $data = Notapengiriman::find($request->id);
        $data->tanggal = date_format(date_create($request->tanggal),"Y-m-d");
        $data->penimbang = $request->penimbang;
        $data->notruk = $request->notruk;
        $data->item = $request->item;
        $data->colly = $request->colly;
        $data->netto = $request->netto;
        $data->units = $request->unit;
        $data->location = $request->location;
        $data->pedagang = $request->pedagang;
        $data->bruto = $request->bruto;
        $data->tara = $request->tara;
        $pack = $request->kodepackage;
        $jml = $request->jumlahpackage;
        $lpack='';
        for($a=0;$a<count($pack);$a++){
            $lpack=$lpack.$pack[$a].':'.$jml[$a].';';
        }
        $data->kodepackage = $lpack;
        $data->kodetimbangan = $request->kodetimbangan;
        if($data->save()){
            return json_encode('Data telah disimpan.');
        } else {
            return json_encode('Data gagal disimpan.');
        }
    }

    public function destroy(request $request)
    {
        $Warehouse = Notapengiriman::find($request->id);
        if($Warehouse->delete())
            {
                return json_encode('Data telah dihapus');
            } else {
                return json_encode('Data gagal dihapus');
            }
    }

    public function cetaksemua(Request $request)
    {
        $header = Notapengiriman::find($request->id);
        $data = Datatimbangan::where('kodetimbangan',$header->kodetimbangan)->leftJoin('item','datatimbangan.iditem','item.id')->first();
        $timbangan = Timbangan::where('kodetimbangan',$header->kodetimbangan)->get();
        $total = Timbangan::where('kodetimbangan',$header->kodetimbangan)->count();
        $jumlah = Timbangan::where('kodetimbangan',$header->kodetimbangan)->sum('quantitydetails');
        return view('cetaknota',['timbangan'=>$timbangan, 'total'=>$total, 'jumlah'=>$jumlah, 'data'=>$data, 'header'=>$header]);
    }
}
