<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Timbangan;
use App\Models\Datatimbangan;

use Auth;

class TimbanganController extends Controller
{
    public function index()
    {
        $user = Auth::user()->name;
        return view('timbangan', compact('user'));
    }

    public function baru()
    {
        $user = Auth::user()->name;
        return view('timbanganbaru', compact('user'));
    }

    public function editpage(Request $request)
    {
        $user = Auth::user()->name;
        $datatimbangan=Datatimbangan::where('kodetimbangan',$request->kode)->first();
        return view('timbanganedit', ['user'=>$user,'datatimbangan'=>$datatimbangan]);
    }

    public function bulkstore(Request $request)
    {
        $kode=Auth::user()->kodegudang.'-'.date('YmdHi').'-'.Auth::user()->fullname;
        $datas= new Datatimbangan;
        $datas->kodetimbangan = $kode;
        $datas->tipe=$request->tipe;
        $datas->pedagang=$request->pedagang;
        $datas->nopol=$request->notruk;
        $datas->iditem=$request->item;
        $datas->created_by = Auth::user()->fullname;
        $datas->save();
        for($i=0;$i<count($request->data);$i++){
            $data = new Timbangan;
            $data->kodetimbangan=$kode;
            $data->quantitydetails=(float)$request->data[$i][1];
            $data->timedetails = $request->data[$i][2];
            $data->save();
        }
        echo "Data telah disimpan di server.";
    }

    public function detail(request $request)
    {
        $data = Timbangan::find($request->id);
        return json_encode($data);
    }

    public function data(Request $request)
    {
        $awal = isset($request->awal)?$request->awal.' 00:00':date('Y-m-d').' 00:00';
        $akhir = isset($request->akhir)?$request->akhir.' 23:59':date('Y-m-d').' 23:59';
        $data = Timbangan::selectRaw("
        timbangan.kodetimbangan, 
        count(timbangan.id) as totaldata, 
        round(sum(quantitydetails),2) as bruto, 
        datatimbangan.pedagang,
        datatimbangan.nopol,
        datatimbangan.tipe,
        item.namaitem,
        date_format(timbangan.created_at,'%d-%m-%Y %H:%i') as date")
        ->leftJoin('datatimbangan','timbangan.kodetimbangan','datatimbangan.kodetimbangan')
        ->leftJoin('item','datatimbangan.iditem','item.id')
        ->whereBetween('timbangan.created_at',[$awal,$akhir])
        ->groupBy('kodetimbangan')
        ->orderBy('kodetimbangan','desc')
        ->get();
        return json_encode(['data'=>$data]);
    }
    public function datadetail(Request $request)
    {
        $kode=$request->kode;
        $data = Timbangan::selectRaw("quantitydetails as qty, date_format(created_at,'%Y-%m-%d %H:%i:%s') as date")
        ->where('kodetimbangan',$kode)->orderBy('id','asc')->get();
        return json_encode($data);
    }

    public function edit(request $request)
    {
        $kode=$request->kode;
        $datasa= Datatimbangan::where('kodetimbangan',$kode)->first();
        if ($datasa === null) {
            $datas=new Datatimbangan;
            $datas->kodetimbangan=$kode;
            $datas->created_by=Auth::user()->fullname;
         } else {
             $datas=$datasa;
         }
        $datas->tipe=$request->tipe;
        $datas->pedagang=$request->pedagang;
        $datas->nopol=$request->notruk;
        $datas->iditem=$request->item;
        $datas->save();

        $datas = Timbangan::where('kodetimbangan',$kode)->get();
        foreach($datas as $d){
            $d->delete();
        }
        for($i=0;$i<count($request->data);$i++){
            $data = new Timbangan;
            $data->kodetimbangan=$kode;
            $data->quantitydetails=$request->data[$i][1];
            $data->timedetails = $request->data[$i][2];
            $data->save();
        }
        echo "Data telah disimpan di server.";
    }

    public function destroy(request $request)
    {
        $counter=0;
        $datas = Timbangan::where('kodetimbangan',$request->id)->get();
        $da = Datatimbangan::where('kodetimbangan', $request->id)->delete();
        foreach($datas as $d){
            if($d->delete()){
                $counter++;
            }
        }
        if($counter>0)
            {
                return json_encode('Data telah dihapus');
            } else {
                return json_encode('Data gagal dihapus');
            }
    }

    public function cetaksemua(Request $request)
    {
        $data = Datatimbangan::where('kodetimbangan',$request->kode)->leftJoin('item','datatimbangan.iditem','item.id')->first();
        $timbangan = Timbangan::where('kodetimbangan',$request->kode)->get();
        $total = Timbangan::where('kodetimbangan',$request->kode)->count();
        $jumlah = Timbangan::where('kodetimbangan',$request->kode)->sum('quantitydetails');
        return view('cetaktimbangan',['timbangan'=>$timbangan, 'total'=>$total, 'jumlah'=>$jumlah, 'data'=>$data]);
    }

    public function cetaksatuan(Request $request)
    {
        return view('cetaktimbangansatuan');
    }

    public function cetakacum(Request $request)
    {
        return view('cetaktimbanganacum');
    }
}
