<?php

namespace App\Models;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use auth;

class User extends Authenticatable
{
    use HasFactory, Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name',
        'email',
        'password',
        'photo',
        'role',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password',
        'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    public function adminlte_image()
    {
        $photo = Auth::user()->photo;
        if($photo == null){
            $link = asset('avatar/user default.png');
        } else {
            $photo = "avatar/".$photo;
            $link = asset($photo);
        }
        return $link;
    }

    public function adminlte_header()
    {
        return ;
    }

    public function adminlte_desc()
    {
        $return = auth()->user()->fullname.' ('.auth()->user()->userlevel.")";
        return $return;
    }

    public function adminlte_profile_url()
    {
        return 'profile';
    }
}
