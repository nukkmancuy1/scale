<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Notapengiriman extends Model
{
    use SoftDeletes;
    use HasFactory;
    protected $table = 'notapengiriman';
    protected $primarykey='id';
}
