<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Timbangan extends Model
{
    use SoftDeletes;
    use HasFactory;
    protected $table = 'timbangan';
    protected $primarykey='id';
}
