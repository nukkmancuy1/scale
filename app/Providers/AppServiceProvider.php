<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        $this->register();
        \Gate::define('superadmin', function () {
            if (auth()->user()->role == 'Superadmin') {
                return true;
            }
            return false;
        });
        \Gate::define('penimbang', function () {
            if (auth()->user()->role == 'Penimbang') {
                return true;
            }
            return false;
        });
        \Gate::define('pembelian', function () {
            if (auth()->user()->role == 'Pembelian') {
                return true;
            }
            return false;
        });
        \Gate::define('staff', function () {
            if (auth()->user()->role == 'Staff') {
                return true;
            }
            return false;
        });
    }
}
