-- phpMyAdmin SQL Dump
-- version 4.9.1
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Waktu pembuatan: 08 Jan 2022 pada 00.26
-- Versi server: 10.4.8-MariaDB
-- Versi PHP: 7.3.11

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `scale`
--

-- --------------------------------------------------------

--
-- Struktur dari tabel `datatimbangan`
--

CREATE TABLE `datatimbangan` (
  `id` int(11) NOT NULL,
  `kodetimbangan` varchar(255) DEFAULT NULL,
  `pedagang` varchar(255) DEFAULT NULL,
  `nopol` varchar(255) DEFAULT NULL,
  `iditem` int(11) DEFAULT NULL,
  `created_by` varchar(255) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `deleted_at` varchar(255) DEFAULT NULL,
  `tipe` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data untuk tabel `datatimbangan`
--

INSERT INTO `datatimbangan` (`id`, `kodetimbangan`, `pedagang`, `nopol`, `iditem`, `created_by`, `created_at`, `updated_at`, `deleted_at`, `tipe`) VALUES
(1, 'W0001-202111190742-admins', 'Pedagang 1', 'B 1223 AB', 2, 'admins', '2021-11-19 07:42:18', '2021-11-19 08:56:29', NULL, 'Penerimaan'),
(2, 'W0001-202111141421-admins', 'Pedagang 1', 'B 1223 AA', 3, 'admins', '2021-11-19 09:21:03', '2021-11-19 09:21:03', NULL, 'Penerimaan'),
(3, 'W0001-202111141148-edy wh01', 'Pedagang 1', 'B1234AS', 2, 'admins', '2021-11-19 09:23:03', '2021-11-19 09:23:03', NULL, 'Penerimaan'),
(4, 'W0001-202111141116-admins', 'Pedagang 1', 'B 1223 AA', 2, 'admins', '2021-11-19 09:31:07', '2021-11-19 09:31:07', NULL, 'Pengiriman'),
(5, 'W0001-202111141114-admins', 'Pedagang 1', 'B 1223 AB', 4, 'admins', '2021-11-19 09:31:35', '2021-11-19 09:31:35', NULL, 'Pengiriman'),
(6, 'W0001-202111140945-admins', 'Pedagang 1', 'B 1223 AB', 2, 'admins', '2021-11-19 09:31:59', '2021-11-19 09:31:59', NULL, 'Pengiriman'),
(7, 'W0001-202111191035-admins', 'Pedagang 2', 'B 1223 AC', 2, 'admins', '2021-11-19 10:35:19', '2021-11-19 11:03:35', '2021-11-19 11:03:35', 'Penerimaan'),
(8, 'W0001-202111270934-admins', 'Pedagang 1', 'B 1223 AB', 2, 'admins', '2021-11-27 09:34:52', '2021-11-27 09:34:52', NULL, 'Penerimaan'),
(9, 'W0001-202201071631-admins', 'Pedagang 1', 'B1234AS', 2, 'admins', '2022-01-07 16:31:29', '2022-01-07 16:31:29', NULL, 'Penerimaan'),
(10, 'W0001-202201071705-admins', 'Pedagang 1', 'B 1223 AB', 2, 'admins', '2022-01-07 17:05:15', '2022-01-07 17:05:15', NULL, 'Pengiriman');

-- --------------------------------------------------------

--
-- Struktur dari tabel `hitspenerimaan`
--

CREATE TABLE `hitspenerimaan` (
  `id` int(11) NOT NULL,
  `idasal` int(11) DEFAULT NULL,
  `createdfrom` varchar(255) DEFAULT NULL,
  `nomor` varchar(255) DEFAULT NULL,
  `tanggal` date DEFAULT NULL,
  `penimbang` varchar(255) DEFAULT NULL,
  `notruk` varchar(255) DEFAULT NULL,
  `item` varchar(255) DEFAULT NULL,
  `colly` float DEFAULT NULL,
  `qty` float DEFAULT NULL,
  `units` varchar(255) DEFAULT NULL,
  `serial` varchar(255) DEFAULT NULL,
  `status` varchar(255) DEFAULT NULL,
  `noitemfulfillment` varchar(255) DEFAULT NULL,
  `pedagang` varchar(255) DEFAULT NULL,
  `location` varchar(255) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `deleted_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data untuk tabel `hitspenerimaan`
--

INSERT INTO `hitspenerimaan` (`id`, `idasal`, `createdfrom`, `nomor`, `tanggal`, `penimbang`, `notruk`, `item`, `colly`, `qty`, `units`, `serial`, `status`, `noitemfulfillment`, `pedagang`, `location`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 1, 'Purchase Order', '1', '2022-01-07', 'admins', 'B1234AS', 'Item 1', 2, 20, 'KG', '1', NULL, NULL, 'Pedagang 1', 'W0001', '2022-01-07 16:39:39', '2022-01-07 16:41:09', NULL),
(2, 1, 'Purchase Order', '2', '2022-01-07', 'admins', 'B1234AS', 'Item 1', 1, 3, 'KG', '2', NULL, NULL, 'Pedagang 1', 'W0001', '2022-01-07 16:45:03', '2022-01-07 16:45:03', NULL);

-- --------------------------------------------------------

--
-- Struktur dari tabel `hitspengiriman`
--

CREATE TABLE `hitspengiriman` (
  `id` int(11) NOT NULL,
  `idasal` int(11) DEFAULT NULL,
  `createdfrom` varchar(255) DEFAULT NULL,
  `nomor` varchar(255) DEFAULT NULL,
  `tanggal` date DEFAULT NULL,
  `penimbang` varchar(255) DEFAULT NULL,
  `notruk` varchar(255) DEFAULT NULL,
  `item` varchar(255) DEFAULT NULL,
  `colly` float DEFAULT NULL,
  `qty` float DEFAULT NULL,
  `units` varchar(255) DEFAULT NULL,
  `serial` varchar(255) DEFAULT NULL,
  `status` varchar(255) DEFAULT NULL,
  `noitemfulfillment` varchar(255) DEFAULT NULL,
  `pedagang` varchar(255) DEFAULT NULL,
  `location` varchar(255) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `deleted_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data untuk tabel `hitspengiriman`
--

INSERT INTO `hitspengiriman` (`id`, `idasal`, `createdfrom`, `nomor`, `tanggal`, `penimbang`, `notruk`, `item`, `colly`, `qty`, `units`, `serial`, `status`, `noitemfulfillment`, `pedagang`, `location`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 1, 'Purchase Order', '1', '2022-01-07', 'admins', 'B 1223 AB', 'Item 1', 2, 13, 'Pilih Unit', '1', NULL, NULL, 'Pedagang 1', 'W0001', '2022-01-07 17:23:48', '2022-01-07 17:23:48', NULL);

-- --------------------------------------------------------

--
-- Struktur dari tabel `item`
--

CREATE TABLE `item` (
  `id` int(11) NOT NULL,
  `kodeitem` varchar(255) DEFAULT NULL,
  `namaitem` varchar(255) DEFAULT NULL,
  `sample` int(11) DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `deleted_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data untuk tabel `item`
--

INSERT INTO `item` (`id`, `kodeitem`, `namaitem`, `sample`, `created_by`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, '3', 'Item 12', NULL, 1, '2021-11-13 10:25:47', '2021-11-13 10:29:37', '2021-11-13 10:29:37'),
(2, 'I0001', 'Item 1', 3, 1, '2021-11-13 12:45:45', '2021-11-13 12:45:45', NULL),
(3, 'I0002', 'Item 2', 2, 1, '2021-11-13 12:46:05', '2021-11-13 12:46:05', NULL),
(4, 'I0003', 'Item 3', 4, 1, '2021-11-13 12:47:44', '2021-11-13 12:49:41', NULL);

-- --------------------------------------------------------

--
-- Struktur dari tabel `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data untuk tabel `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(4, '2014_10_12_200000_add_two_factor_columns_to_users_table', 2),
(5, '2019_12_14_000001_create_personal_access_tokens_table', 2),
(6, '2020_11_21_075303_create_sessions_table', 2),
(7, '2014_10_12_000000_create_users_table', 3),
(8, '2014_10_12_100000_create_password_resets_table', 3),
(9, '2019_08_19_000000_create_failed_jobs_table', 3);

-- --------------------------------------------------------

--
-- Struktur dari tabel `notapenerimaan`
--

CREATE TABLE `notapenerimaan` (
  `id` int(11) NOT NULL,
  `tanggal` date DEFAULT NULL,
  `penimbang` varchar(255) DEFAULT NULL,
  `notruk` varchar(255) DEFAULT NULL,
  `item` varchar(255) DEFAULT NULL,
  `colly` varchar(255) DEFAULT NULL,
  `netto` float(255,0) DEFAULT NULL,
  `units` varchar(255) DEFAULT NULL,
  `location` varchar(255) DEFAULT NULL,
  `statusdata` varchar(255) DEFAULT NULL,
  `pedagang` varchar(255) DEFAULT NULL,
  `bruto` float(255,0) DEFAULT NULL,
  `tara` float(255,0) DEFAULT NULL,
  `kodepackage` text DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `deleted_at` datetime DEFAULT NULL,
  `kodetimbangan` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data untuk tabel `notapenerimaan`
--

INSERT INTO `notapenerimaan` (`id`, `tanggal`, `penimbang`, `notruk`, `item`, `colly`, `netto`, `units`, `location`, `statusdata`, `pedagang`, `bruto`, `tara`, `kodepackage`, `created_at`, `updated_at`, `deleted_at`, `kodetimbangan`) VALUES
(1, '2022-01-07', 'admins', 'B1234AS', 'Item 1', '3', 23, 'KG', 'W0001', NULL, 'Pedagang 1', 35, 12, '3:1;2:2;', '2022-01-07 16:36:28', '2022-01-07 16:36:28', NULL, 'W0001-202201071631-admins');

-- --------------------------------------------------------

--
-- Struktur dari tabel `notapengiriman`
--

CREATE TABLE `notapengiriman` (
  `id` int(11) NOT NULL,
  `tanggal` date DEFAULT NULL,
  `penimbang` varchar(255) DEFAULT NULL,
  `notruk` varchar(255) DEFAULT NULL,
  `item` varchar(255) DEFAULT NULL,
  `colly` varchar(255) DEFAULT NULL,
  `netto` float(255,0) DEFAULT NULL,
  `units` varchar(255) DEFAULT NULL,
  `location` varchar(255) DEFAULT NULL,
  `statusdata` varchar(255) DEFAULT NULL,
  `pedagang` varchar(255) DEFAULT NULL,
  `bruto` float(255,0) DEFAULT NULL,
  `tara` float(255,0) DEFAULT NULL,
  `kodepackage` text DEFAULT NULL,
  `kodetimbangan` varchar(255) NOT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `deleted_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data untuk tabel `notapengiriman`
--

INSERT INTO `notapengiriman` (`id`, `tanggal`, `penimbang`, `notruk`, `item`, `colly`, `netto`, `units`, `location`, `statusdata`, `pedagang`, `bruto`, `tara`, `kodepackage`, `kodetimbangan`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, '2022-01-07', 'admins', 'B 1223 AB', 'Item 1', '2', 13, 'Pilih Unit', 'W0001', NULL, 'Pedagang 1', 21, 8, '3:1;2:1;', 'W0001-202201071705-admins', '2022-01-07 17:09:29', '2022-01-07 17:09:29', NULL);

-- --------------------------------------------------------

--
-- Struktur dari tabel `package`
--

CREATE TABLE `package` (
  `id` int(11) NOT NULL,
  `kodepackage` varchar(255) DEFAULT NULL,
  `namapackage` varchar(255) DEFAULT NULL,
  `beratpackage` varchar(255) DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `deleted_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data untuk tabel `package`
--

INSERT INTO `package` (`id`, `kodepackage`, `namapackage`, `beratpackage`, `created_by`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 'P0001', 'Package 1', '100', 1, '2021-11-13 10:45:53', '2021-11-13 10:47:18', '2021-11-13 10:47:18'),
(2, 'P0002', 'Package 2', '2', 1, '2021-11-13 10:58:02', '2021-11-21 10:56:06', NULL),
(3, 'P0001', 'Package 1', '2', 1, '2021-11-21 08:14:53', '2021-11-21 08:14:53', NULL);

-- --------------------------------------------------------

--
-- Struktur dari tabel `password_resets`
--

CREATE TABLE `password_resets` (
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Struktur dari tabel `timbangan`
--

CREATE TABLE `timbangan` (
  `id` int(11) NOT NULL,
  `quantitydetails` float DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `kodetimbangan` varchar(255) DEFAULT NULL,
  `deleted_at` datetime DEFAULT NULL,
  `timedetails` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data untuk tabel `timbangan`
--

INSERT INTO `timbangan` (`id`, `quantitydetails`, `created_at`, `updated_at`, `kodetimbangan`, `deleted_at`, `timedetails`) VALUES
(1, 10, '2022-01-07 16:31:29', '2022-01-07 16:31:29', 'W0001-202201071631-admins', NULL, '2022-01-07 23:30:48'),
(2, 12, '2022-01-07 16:31:29', '2022-01-07 16:31:29', 'W0001-202201071631-admins', NULL, '2022-01-07 23:31:05'),
(3, 13, '2022-01-07 16:31:29', '2022-01-07 16:31:29', 'W0001-202201071631-admins', NULL, '2022-01-07 23:31:14'),
(4, 11, '2022-01-07 17:05:15', '2022-01-07 17:05:15', 'W0001-202201071705-admins', NULL, '2022-01-08 00:04:09'),
(5, 10, '2022-01-07 17:05:15', '2022-01-07 17:05:15', 'W0001-202201071705-admins', NULL, '2022-01-08 00:04:50');

-- --------------------------------------------------------

--
-- Struktur dari tabel `users`
--

CREATE TABLE `users` (
  `id` int(11) NOT NULL,
  `fullname` varchar(255) DEFAULT NULL,
  `password` varchar(255) DEFAULT NULL,
  `userlevel` varchar(255) DEFAULT NULL,
  `kodegudang` varchar(255) DEFAULT NULL,
  `remember_token` varchar(255) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `email` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data untuk tabel `users`
--

INSERT INTO `users` (`id`, `fullname`, `password`, `userlevel`, `kodegudang`, `remember_token`, `created_at`, `updated_at`, `email`) VALUES
(1, 'admins', '$2y$10$VX6IvWJ9KY2uK93NkzGUju/Q0Xi7VkY2.P41RFFLZBXBaVZeVDgUa', 'Administrator', 'W0001', 'wezaYyPZq1AX6gKmjGepRyhXDWbHGNz6DyQRxJPLyDHnKtB1Q5XdjgVmoiR7', '2020-11-21 10:07:55', '2021-11-13 12:10:18', 'admin@mail.com'),
(2, 'Penimbang', '$2y$10$4AViphaR4m6BiGYLtFY6CuXMkt9uJHYCMtCL6CJZAJ8lrBMsMDa66', 'Penimbang', 'W0001', NULL, '2021-11-13 11:59:41', '2021-11-13 12:09:37', 'penimbang@mail.com'),
(3, 'Staff Warehouse 1', '$2y$10$opNNHI5zUSfaV50We9/9cuFgQDjJ6VhVy0gQJUewCJ41K3bVU8WAa', 'Staff', 'W0001', NULL, '2021-11-13 12:00:31', '2021-11-13 12:13:50', 'staff@mail.com'),
(4, 'Penimbang 2', '$2y$10$cGQ6fFhdfn/lUy/Ui4Yuj.AinxhyXaGjDw7vL782cclnplk2Pi4fe', 'Penimbang', 'W0002', NULL, '2021-11-13 12:01:50', '2021-11-13 12:01:50', 'penimbang2@mail.com'),
(6, 'Penimbang 3', '$2y$10$5J2k2AlBxRTGg3FtKYwhAusnx5XcHIAg5UsMuDWzH1uoWTYuyqNwK', 'Penimbang', 'W0003', NULL, '2021-11-13 12:15:27', '2021-11-13 12:15:27', 'penimbang3@mail.com'),
(7, 'Staff Warehouse 2', '$2y$10$qB3Hw2ZwpgFlTHQO9rZE1uIVs7cHR/6zOeDpIiYJYzqUkWKRvc3RK', 'Penimbang', 'W0002', NULL, '2021-11-13 12:16:25', '2021-11-13 12:16:25', 'staff2@mail.com'),
(8, 'edy wh01', '$2y$10$.JgQpQG9umCwFjTRmesLMupG8q1IJQ1nLVOFie6hn1hI9CICA5cVu', 'Penimbang', 'W0001', NULL, '2021-11-14 11:45:46', '2021-11-14 11:45:46', 'edywh01@mail.com'),
(9, 'edy wh02', '$2y$10$42ZT1uKPeFKdCETRkIaMqezaSiMqYYVFxrDh6W4Ouaws8SiAAYmh2', 'Penimbang', 'W0002', NULL, '2021-11-14 11:46:13', '2021-11-14 11:46:13', 'edywh02@mail.com');

-- --------------------------------------------------------

--
-- Struktur dari tabel `warehouse`
--

CREATE TABLE `warehouse` (
  `id` int(11) NOT NULL,
  `kodegudang` varchar(255) DEFAULT NULL,
  `namagudang` varchar(255) DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `deleted_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data untuk tabel `warehouse`
--

INSERT INTO `warehouse` (`id`, `kodegudang`, `namagudang`, `created_by`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 'W0001', 'Warehouse 1', 1, '2021-11-13 08:04:38', '2021-11-14 10:23:52', NULL),
(2, 'W0002', 'Warehouse 2', 1, '2021-11-13 08:07:53', '2021-11-13 08:07:53', NULL),
(3, 'W0003', 'Warehouse 3', 1, '2021-11-13 08:08:53', '2021-11-13 12:50:41', NULL),
(4, 'W0004', 'Warehouse 54', 1, '2021-11-13 08:09:54', '2021-11-13 08:30:33', NULL);

--
-- Indexes for dumped tables
--

--
-- Indeks untuk tabel `datatimbangan`
--
ALTER TABLE `datatimbangan`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `hitspenerimaan`
--
ALTER TABLE `hitspenerimaan`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `hitspengiriman`
--
ALTER TABLE `hitspengiriman`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `item`
--
ALTER TABLE `item`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `notapenerimaan`
--
ALTER TABLE `notapenerimaan`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `notapengiriman`
--
ALTER TABLE `notapengiriman`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `package`
--
ALTER TABLE `package`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `password_resets`
--
ALTER TABLE `password_resets`
  ADD KEY `password_resets_email_index` (`email`);

--
-- Indeks untuk tabel `timbangan`
--
ALTER TABLE `timbangan`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `warehouse`
--
ALTER TABLE `warehouse`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT untuk tabel yang dibuang
--

--
-- AUTO_INCREMENT untuk tabel `datatimbangan`
--
ALTER TABLE `datatimbangan`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT untuk tabel `hitspenerimaan`
--
ALTER TABLE `hitspenerimaan`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT untuk tabel `hitspengiriman`
--
ALTER TABLE `hitspengiriman`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT untuk tabel `item`
--
ALTER TABLE `item`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT untuk tabel `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;

--
-- AUTO_INCREMENT untuk tabel `notapenerimaan`
--
ALTER TABLE `notapenerimaan`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT untuk tabel `notapengiriman`
--
ALTER TABLE `notapengiriman`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT untuk tabel `package`
--
ALTER TABLE `package`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT untuk tabel `timbangan`
--
ALTER TABLE `timbangan`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT untuk tabel `users`
--
ALTER TABLE `users`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;

--
-- AUTO_INCREMENT untuk tabel `warehouse`
--
ALTER TABLE `warehouse`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
