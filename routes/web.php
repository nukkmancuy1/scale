<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('auth/login');
});

Auth::routes();

Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');

Route::get('/profile', [App\Http\Controllers\ProfileController::class, 'index'])->name('profile');
Route::get('image-upload', [ App\Http\Controllers\ProfileController::class, 'imageUpload' ])->name('image.upload');
Route::post('image-upload', [ App\Http\Controllers\ProfileController::class, 'imageUploadPost' ])->name('image.upload.post');
Route::post('changepassword', [ App\Http\Controllers\ProfileController::class, 'changepassword' ])->name('changepassword');
Route::post('hapusakun', [ App\Http\Controllers\ProfileController::class, 'destroy' ])->name('hapusakun');

Route::get('/user', [App\Http\Controllers\UserController::class, 'index'])->name('user');
Route::post('/userbaru', [App\Http\Controllers\UserController::class, 'create'])->name('userbaru');
Route::post('/hapususer', [App\Http\Controllers\UserController::class, 'destroy'])->name('hapususer');
Route::get('/userdata', [App\Http\Controllers\UserController::class, 'show'])->name('userdata');
Route::post('/edituser', [App\Http\Controllers\UserController::class, 'edit'])->name('edituser');
Route::get('/user/data', [App\Http\Controllers\UserController::class, 'data'])->name('datauser');
Route::post('/edituserpassword', [App\Http\Controllers\UserController::class, 'editpassword'])->name('edituserpassword');

Route::get('/warehouse', [App\Http\Controllers\WarehouseController::class, 'index'])->name('warehouse');
Route::post('/warehouse/baru', [App\Http\Controllers\WarehouseController::class, 'create'])->name('warehousebaru');
Route::post('/warehouse/hapus', [App\Http\Controllers\WarehouseController::class, 'destroy'])->name('hapuswarehouse');
Route::get('/warehouse/detail', [App\Http\Controllers\WarehouseController::class, 'detail'])->name('warehousedetail');
Route::get('/warehouse/data', [App\Http\Controllers\WarehouseController::class, 'data'])->name('warehousedata');
Route::post('/warehouse/edit', [App\Http\Controllers\WarehouseController::class, 'edit'])->name('editwarehouse');

Route::get('/item', [App\Http\Controllers\ItemController::class, 'index'])->name('item');
Route::post('/item/baru', [App\Http\Controllers\ItemController::class, 'create'])->name('itembaru');
Route::post('/item/hapus', [App\Http\Controllers\ItemController::class, 'destroy'])->name('hapusitem');
Route::get('/item/detail', [App\Http\Controllers\ItemController::class, 'detail'])->name('itemdetail');
Route::get('/item/data', [App\Http\Controllers\ItemController::class, 'data'])->name('itemdata');
Route::post('/item/edit', [App\Http\Controllers\ItemController::class, 'edit'])->name('edititem');

Route::get('/package', [App\Http\Controllers\PackageController::class, 'index'])->name('package');
Route::post('/package/baru', [App\Http\Controllers\PackageController::class, 'create'])->name('packagebaru');
Route::post('/package/hapus', [App\Http\Controllers\PackageController::class, 'destroy'])->name('hapuspackage');
Route::get('/package/detail', [App\Http\Controllers\PackageController::class, 'detail'])->name('packagedetail');
Route::get('/package/data', [App\Http\Controllers\PackageController::class, 'data'])->name('packagedata');
Route::post('/package/edit', [App\Http\Controllers\PackageController::class, 'edit'])->name('editpackage');

Route::get('/notapenerimaan', [App\Http\Controllers\NotapenerimaanController::class, 'index'])->name('notapenerimaan');
Route::post('/notapenerimaan/baru', [App\Http\Controllers\NotapenerimaanController::class, 'create'])->name('notapenerimaanbaru');
Route::post('/notapenerimaan/hapus', [App\Http\Controllers\NotapenerimaanController::class, 'destroy'])->name('hapusnotapenerimaan');
Route::get('/notapenerimaan/detail', [App\Http\Controllers\NotapenerimaanController::class, 'detail'])->name('notapenerimaandetail');
Route::get('/notapenerimaan/data', [App\Http\Controllers\NotapenerimaanController::class, 'data'])->name('notapenerimaandata');
Route::post('/notapenerimaan/edit', [App\Http\Controllers\NotapenerimaanController::class, 'edit'])->name('editnotapenerimaan');
Route::get('/notapenerimaan/cetaksemua', [App\Http\Controllers\NotapenerimaanController::class, 'cetaksemua'])->name('cetaknotapenerimaan');

Route::get('/notapengiriman', [App\Http\Controllers\NotapengirimanController::class, 'index'])->name('notapengiriman');
Route::post('/notapengiriman/baru', [App\Http\Controllers\NotapengirimanController::class, 'create'])->name('notapengirimanbaru');
Route::post('/notapengiriman/hapus', [App\Http\Controllers\NotapengirimanController::class, 'destroy'])->name('hapusnotapengiriman');
Route::get('/notapengiriman/detail', [App\Http\Controllers\NotapengirimanController::class, 'detail'])->name('notapengirimandetail');
Route::get('/notapengiriman/data', [App\Http\Controllers\NotapengirimanController::class, 'data'])->name('notapengirimandata');
Route::post('/notapengiriman/edit', [App\Http\Controllers\NotapengirimanController::class, 'edit'])->name('editnotapengiriman');
Route::get('/notapengiriman/cetaksemua', [App\Http\Controllers\NotapengirimanController::class, 'cetaksemua'])->name('cetaknotapengiriman');

Route::get('/timbangan', [App\Http\Controllers\TimbanganController::class, 'index'])->name('timbangan');
Route::get('/timbangan/baru', [App\Http\Controllers\TimbanganController::class, 'baru'])->name('timbanganbaru');
Route::get('/timbangan/edit', [App\Http\Controllers\TimbanganController::class, 'editpage'])->name('timbanganedit');
Route::post('/timbangan/simpan', [App\Http\Controllers\TimbanganController::class, 'create'])->name('timbangansimpan');
Route::post('/timbangan/hapus', [App\Http\Controllers\TimbanganController::class, 'destroy'])->name('hapustimbangan');
Route::get('/timbangan/detail', [App\Http\Controllers\TimbanganController::class, 'detail'])->name('timbangandetail');
Route::get('/timbangan/detailbykode', [App\Http\Controllers\TimbanganController::class, 'datadetail'])->name('timbangankode');
Route::get('/timbangan/data', [App\Http\Controllers\TimbanganController::class, 'data'])->name('timbangandata');
Route::post('/timbangan/simpanedit', [App\Http\Controllers\TimbanganController::class, 'edit'])->name('edittimbangan');
Route::post('/timbangan/bulkstore', [App\Http\Controllers\TimbanganController::class, 'bulkstore'])->name('bulkstoretimbangan');
Route::get('/timbangan/cetaksemua', [App\Http\Controllers\TimbanganController::class, 'cetaksemua'])->name('cetaksemua');
Route::get('/timbangan/cetaksatuan', [App\Http\Controllers\TimbanganController::class, 'cetaksatuan'])->name('cetaksatuan');
Route::get('/timbangan/cetakacum', [App\Http\Controllers\TimbanganController::class, 'cetakacum'])->name('cetakacum');

Route::get('/hitpenerimaan', [App\Http\Controllers\HitsController::class, 'hitpenerimaan'])->name('hitpenerimaan');
Route::get('/hitpenerimaan/data', [App\Http\Controllers\HitsController::class, 'datahitpenerimaan'])->name('datahitpenerimaan');
Route::post('/hitpenerimaan/baru', [App\Http\Controllers\HitsController::class, 'create'])->name('hitspenerimaanbaru');
Route::get('/hitpenerimaan/detail', [App\Http\Controllers\HitsController::class, 'detail'])->name('hitspenerimaandetail');
Route::get('/hitpenerimaan/replace', [App\Http\Controllers\HitsController::class, 'replace'])->name('replacehitspenerimaan');
Route::post('/hitpenerimaan/hapus', [App\Http\Controllers\HitsController::class, 'destroy'])->name('hapushitpenerimaan');
Route::post('/hitpenerimaan/simpanedit', [App\Http\Controllers\HitsController::class, 'edit'])->name('edithitpenerimaan');
Route::get('/hitpenerimaan/ubahstatus', [App\Http\Controllers\HitsController::class, 'ubahstatus'])->name('editstatushitpenerimaan');

Route::get('/hitpengiriman', [App\Http\Controllers\HitspengirimanController::class, 'hitpengiriman'])->name('hitpengiriman');
Route::get('/hitpengiriman/data', [App\Http\Controllers\HitspengirimanController::class, 'datahitpengiriman'])->name('datahitpengiriman');
Route::post('/hitpengiriman/baru', [App\Http\Controllers\HitspengirimanController::class, 'create'])->name('hitspengirimanbaru');
Route::get('/hitpengiriman/detail', [App\Http\Controllers\HitspengirimanController::class, 'detail'])->name('hitspengirimandetail');
Route::get('/hitpengiriman/replace', [App\Http\Controllers\HitspengirimanController::class, 'replace'])->name('replacehitspengiriman');
Route::post('/hitpengiriman/hapus', [App\Http\Controllers\HitspengirimanController::class, 'destroy'])->name('hapushitpengiriman');
Route::post('/hitpengiriman/simpanedit', [App\Http\Controllers\HitspengirimanController::class, 'edit'])->name('edithitpengiriman');