@extends('adminlte::page')

@section('title', 'Profile')

@section('content_header')
<h1>Profile</h1>
<div class="row">
    <div class="col-md-12 mt-2">
        @if ($message = Session::get('success'))
        <div class="alert alert-success alert-block">
            <button type="button" class="close" data-dismiss="alert">×</button>
            <strong>{{ $message }}</strong>
        </div>
        @endif
        @if ($message = Session::get('danger'))
        <div class="alert alert-danger alert-block">
            <button type="button" class="close" data-dismiss="alert">×</button>
            <strong>{{ $message }}</strong>
        </div>
        @endif
    </div>
</div>
@stop

@section('content')
<div class="row">
    <div class="col-12 col-md-6">
        <div class="card">
            <div class="card-header"><h4>Informasi Akun</div>
            <!-- /.card-header -->
            <div class="card-body">
                <form method="post" enctype="multipart/form-data" action="{{route('image.upload.post')}}">
                    <div class="row">
                        <div class="col-md-12 mb-2">
                            <label>Nama</label>
                            <input type="text" class="form-control" name="nama" value="{{$user}}" required>
                            <input type="hidden" class="form-control" name="id" value="{{$id}}">
                        </div>
                        <div class="col-md-12 mb-2">
                            <label>Email</label>
                            <input type="text" class="form-control" name="email" value="{{$email}}" required>
                        </div>
                        <div class="col-md-12 mb-2">
                            <input type="submit" class="btn btn-primary" value="Simpan">
                        </div>
                        <div class="col-md-12 mb-2">
                        <hr>
                        Ketika akun anda dihapus, semua data anda akan dihapus. Sebelum menghapus akun anda, mohon untuk mengunduh data atau informasi yang anda butuhkan.
                        <br>
                        <button type="button" class="btn btn-danger" data-toggle="modal" data-target="#modal-hapus"><i class="fas fa-fw fa-trash"></i> Hapus Akun Saya</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
    <div class="col-12 col-md-6">
        <div class="card">
            <div class="card-header"><h4>Ganti Password</div>
            <!-- /.card-header -->
            <div class="card-body">
            <form method="post" action="{{route('changepassword')}}">
                <div class="row">
                    @foreach ($errors->all() as $error)
                            <p class="text-danger">{{ $error }}</p>
                         @endforeach 
                        <div class="col-md-12 mb-2">
                            <label>Password</label>
                            @csrf
                            <input type="password" class="form-control" name="current_password" required>
                            <input type="hidden" class="form-control" name="id" value="{{$id}}">
                        </div>
                        <div class="col-md-12 mb-2">
                            <label>Password Baru</label>
                            <input type="password" class="form-control" name="new_password" required>
                        </div>
                        <div class="col-md-12 mb-2">
                            <label>Konfirmasi Password Baru</label>
                            <input type="password" class="form-control" name="new_confirm_password" required>
                        </div>
                        <div class="col-md-12 mb-2">
                            <input type="submit" class="btn btn-primary" value="Simpan">
                        </div>
                </div>
                </form>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="modal-hapus">
    <div class="modal-dialog modal-sm">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title">Hapus Akun</h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <form method="POST" action="{{ route('hapusakun') }}">
                <div class="modal-body">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="form-group">
                                Apakah anda yakin ingin menghapus akun?
                                @csrf
                                <input type="hidden" class="form-control" name="id" id="hapusid" value="{{$id}}">
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Tidak</button>
                    <button type="submit" class="btn btn-danger">Ya</button>
                </div>
            </form>
        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>
<!-- /.modal -->
@stop

@section('css')
<link rel="stylesheet" href="/css/admin_custom.css">
@stop

@section('js')
<script> console.log('Hi!'); </script>
@stop