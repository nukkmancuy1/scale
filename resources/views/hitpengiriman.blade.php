@extends('adminlte::page')

@section('title', 'Hit Pengiriman')

@section('content_header')
<h1>Hit Pengiriman</h1>
@stop

@section('content')
<div class="row">
    <div class="col-12">
        <div class="card">
            <!-- /.card-header -->
            <div class="card-header">
                <div class="row">
                    <div class="col-xs-12 col-md-8">&nbsp;</div>
                    <div class="col-xs-12 col-md-1 pt-2">
                        Periode : 
                    </div>
                    <div class="col-xs-12 col-md-3">
                        <input type="text" class="form-control" id="daterangepick">
                    </div>
                </div>
            </div>
            <div class="card-body">
                <table id="example1" class="table table-bordered table-striped dataTable" width="100%">
                    <thead>
                        <tr role="row">
                            <th width="5%">#</th>
                            <th>Jenis Pengiriman</th>
                            <th>Nomor</th>
                            <th>Tanggal</th>
                            <th>Pedagang</th>
                            <th>No Truk</th>
                            <th>Item</th>
                            <th>Colly</th>
                            <th>Quantity</th>
                            <th>Dibuat Tanggal</th>
                            <th>Pilihan</th>
                        </tr>
                    </thead>
                </table>
            </div>
        </div>
        <!-- /.card-body -->
    </div>
</div>
</div>

<div class="modal fade" id="modal-xl">
    <div class="modal-dialog modal-xl">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title">Tambahkan hit pengiriman baru</h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <form method="POST" action="{{ route('hitspengirimanbaru') }}" id="savenewdata">
                <div class="modal-body">
                    <div class="row">
                        <div class="col-xs-12 col-md-3 mb-2">
                            <div class="form-group" id="setidasal">
                                <label>Pengiriman</label>
                                <select name="idasal" id="idasal" class="form-control" required onchange="setvalue()">
                                    <option>Pilih Pengiriman</option>
                                    @foreach(DB::select('select sum(hp.qty), np.id, np.tanggal, np.pedagang, np.notruk, np.item, if(sum(hp.qty)>0,np.netto - sum(hp.qty), np.netto) as netto, if(sum(hp.colly)>0,np.colly - sum(hp.colly), np.colly) as colly, np.units from notapengiriman np 
                                    left outer join hitspengiriman hp on np.id=hp.idasal and hp.deleted_at is null
                                    where np.deleted_at is null
                                    group by np.id 
                                    having netto > 0') as $datatimbangan)
                                    <option value="{{$datatimbangan->id}}"
                                    pedagang="{{$datatimbangan->pedagang}}" 
                                    nopol="{{$datatimbangan->notruk}}" 
                                    item="{{$datatimbangan->item}}"
                                    units="{{$datatimbangan->units}}"
                                    colly="{{$datatimbangan->colly}}"
                                    netto="{{$datatimbangan->netto}}"
                                    >
                                        {{$datatimbangan->tanggal}} - {{$datatimbangan->pedagang}} - {{$datatimbangan->notruk}} - {{$datatimbangan->netto}}
                                    </option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                        <div class="col-xs-12 col-md-3 mb-2">
                            <div class="form-group">
                                <label>Pedagang</label>
                                <input type="text" class="form-control" name="pedagang" id="pedagang" readonly required>
                            </div>
                        </div>
                        <div class="col-xs-12 col-md-3 mb-2">
                            <div class="form-group">
                                <label>Nomor Truk</label>
                                <input type="text" class="form-control" name="notruk" id="nopol" readonly required>
                            </div>
                        </div>
                        <div class="col-xs-12 col-md-3 mb-2">
                            <div class="form-group">
                                <label>Item</label>
                                <input type="text" class="form-control" name="item" id="item" readonly required>
                            </div>
                        </div>
                        <div class="col-xs-12 col-md-3 mb-2">
                            <div class="form-group">
                                <label>Dibuat Berdasarkan</label>
                                <select name="createdfrom" class="form-control" required onchange="showfulfillment(this.value)">
                                    <option>Pilih Data</option>
                                    <option value="Purchase Order">Purchase Order</option>
                                    <option value="Transfer Order">Transfer Order</option>
                                </select>
                            </div>
                        </div>
                        <div class="col-xs-12 col-md-3 mb-2" style="display:none;" id="fulfilment">
                            <div class="form-group">
                                <label>No. Item Fulfillment</label>
                                <input type="text" class="form-control" name="noitemfulfillment" id="fulfilinput">
                            </div>
                        </div>
                        <div class="col-xs-12 col-md-3 mb-2">
                            <div class="form-group">
                                <label>Nomor</label>
                                <input type="text" class="form-control" name="nomor" required>
                            </div>
                        </div>
                        <div class="col-xs-12 col-md-3 mb-2">
                            <div class="form-group">
                                <label>Tanggal</label>
                                <input type="date" class="form-control datepicker" name="tanggal" value="{{date('Y-m-d')}}" required>
                            </div>
                        </div>
                        <div class="col-xs-12 col-md-3 mb-2">
                            <div class="form-group">
                                <label>Unit</label>
                                <select name="unit" id="units" class="form-control" required>
                                    <option>Pilih Unit</option>
                                    <option value="KG">KG</option>
                                </select>
                            </div>
                        </div>
                        <div class="col-xs-12 col-md-3 mb-2">
                            <div class="form-group">
                                <label>Serial</label>
                                <input type="text" class="form-control" name="serial" required>
                            </div>
                        </div>
                        <div class="col-xs-12 col-md-3 mb-2">
                            <div class="form-group">
                                <label>Colly</label>
                                <input type="number" class="form-control" name="colly" id="colly" required>
                            </div>
                        </div>
                        <div class="col-xs-12 col-md-3 mb-2">
                            <div class="form-group">
                                <label>Quantity</label>
                                <input type="number" class="form-control" name="qty" id="qty" required>
                            </div>
                        </div>
                        <div class="col-xs-12 col-md-3 mb-2">
                            <div class="form-group">
                                <label>Location</label>
                                <input type="text" class="form-control" name="location" value="{{auth()->user()->kodegudang}}" readonly required>
                            </div>
                        </div>
                        <div class="col-xs-12 col-md-3 mb-2">
                            <div class="form-group">
                                <label>Penimbang</label>
                                @csrf
                                <input type="text" class="form-control" name="penimbang" readonly value="{{auth()->user()->fullname}}" required>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer justify-content-between">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    <button type="submit" class="btn btn-primary">Save</button>
                </div>
            </form>
        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>
<div class="modal fade" id="modal-edit">
    <div class="modal-dialog modal-xl">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title">Ubah data hit pengiriman</h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <form method="POST" action="{{ route('edithitpengiriman') }}" id="editdata">
                <div class="modal-body">
                    <div class="row">
                    <div class="col-xas-12 col-md-3 mb-2">
                            <div class="form-group">
                                <label>Pedagang</label>
                                <input type="text" class="form-control" name="pedagang" id="pedagangedit" readonly required>
                            </div>
                        </div>
                        <div class="col-xas-12 col-md-3 mb-2">
                            <div class="form-group">
                                <label>Nomor Truk</label>
                                <input type="text" class="form-control" name="notruk" id="notrukedit" readonly required>
                            </div>
                        </div>
                        <div class="col-xas-12 col-md-3 mb-2">
                            <div class="form-group">
                                <label>Item</label>
                                <input type="text" class="form-control" name="item" id="itemedit" readonly required>
                            </div>
                        </div>
                        <div class="col-xas-12 col-md-3 mb-2">
                            <div class="form-group">
                                <label>Dibuat Berdasarkan</label>
                                <select name="createdfrom" id="createdfromedit" class="form-control" required onchange="showfulfillmentedit(this.value)">
                                    <option>Pilih Data</option>
                                    <option value="Purchase Order">Purchase Order</option>
                                    <option value="Transfer Order">Transfer Order</option>
                                </select>
                            </div>
                        </div>
                        <div class="col-xas-12 col-md-3 mb-2" style="display:none;" id="fulfilmentedit">
                            <div class="form-group">
                                <label>No. Item Fulfillment</label>
                                <input type="text" class="form-control" name="noitemfulfillment" id="nofulfillmentedit">
                            </div>
                        </div>
                        <div class="col-xas-12 col-md-3 mb-2">
                            <div class="form-group">
                                <label>Nomor</label>
                                <input type="text" class="form-control" name="nomor" id="nomoredit" required>
                                <input type="hidden" class="form-control" name="id" id="idedit" required>
                            </div>
                        </div>
                        <div class="col-xas-12 col-md-3 mb-2">
                            <div class="form-group">
                                <label>Tanggal</label>
                                <input type="date" class="form-control datepicker" name="tanggal" id="tanggaledit" required>
                            </div>
                        </div>                        
                        <div class="col-xas-12 col-md-3 mb-2">
                            <div class="form-group">
                                <label>Unit</label>
                                <select name="unit" class="form-control" id="unitedit" required>
                                    <option>Pilih Unit</option>
                                    <option value="KG">KG</option>
                                </select>
                            </div>
                        </div>
                        <div class="col-xas-12 col-md-3 mb-2">
                            <div class="form-group">
                                <label>Serial</label>
                                <input type="text" class="form-control" name="serial" id="serialedit" required>
                            </div>
                        </div>
                        <div class="col-xas-12 col-md-3 mb-2">
                            <div class="form-group">
                                <label>Colly</label>
                                <input type="number" class="form-control" name="colly" id="collyedit" required>
                            </div>
                        </div>
                        <div class="col-xas-12 col-md-3 mb-2">
                            <div class="form-group">
                                <label>Quantity</label>
                                <input type="text" class="form-control" name="qty" id="qtyedit" required>
                            </div>
                        </div>
                        <div class="col-xas-12 col-md-3 mb-2">
                            <div class="form-group">
                                <label>Location</label>
                                <input type="text" class="form-control" name="location" id="locationedit" value="{{auth()->user()->kodegudang}}" readonly required>
                            </div>
                        </div>
                        <div class="col-xas-12 col-md-3 mb-2">
                            <div class="form-group">
                                <label>Penimbang</label>
                                @csrf
                                <input type="text" class="form-control" name="penimbang" id="penimbangedit" readonly value="{{auth()->user()->fullname}}" required>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer justify-content-between">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    <button type="submit" class="btn btn-primary"  id="simpantbnedit">Save</button>
                </div>
            </form>
        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>
<div class="modal fade" id="modal-hapus">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title">Hapus Data</h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <form method="POST" action="{{ route('hapushitpengiriman') }}" id="hapusdata">
                <div class="modal-body">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="form-group">
                                Apakah anda yakin ingin menghapus data?
                                @csrf
                                <input type="hidden" class="form-control" name="id" id="hapusid">
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Tidak</button>
                    <button type="submit" class="btn btn-danger">Ya</button>
                </div>
            </form>
        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>
<!-- /.modal -->
@stop

@section('css')
<link rel="stylesheet" href="vendor/datatables/css/dataTables.bootstrap4.css">
<link rel="stylesheet" href="vendor/datatables-plugins/buttons/css/buttons.bootstrap4.min.css">
<link rel="stylesheet" href="vendor/daterangepicker/daterangepicker.css">
@stop

@section('js')
<script src="vendor/datatables/js/jquery.dataTables.min.js"></script>
<script src="vendor/datatables/js/dataTables.bootstrap4.min.js"></script>
<script src="vendor/datatables-plugins/buttons/js/buttons.bootstrap4.min.js"></script>
<script src="vendor/datatables-plugins/buttons/js/dataTables.buttons.min.js"></script>
<script src="vendor/datatables-plugins/buttons/js/buttons.colVis.min.js"></script>
<script src="vendor/datatables-plugins/buttons/js/buttons.flash.min.js"></script>
<script src="vendor/datatables-plugins/buttons/js/buttons.html5.min.js"></script>
<script src="vendor/datatables-plugins/buttons/js/buttons.print.min.js"></script>
<script src="vendor/daterangepicker/moment.min.js"></script>
<script src="vendor/daterangepicker/daterangepicker.js"></script>
<script>
    var start = moment();
    var end = moment();
    $(function() {
        function cb(started, ended) {
            $('#daterangepick span').html(started.format('MMMM DD, YYYY') + ' - ' + ended.format('MMMM DD, YYYY'));
            start=started;
            end=ended;
            var awal=start.format('YYYY-MM-DD');
            var akhir=end.format('YYYY-MM-DD');
            table.ajax.url("{{route('datahitpengiriman')}}?awal="+awal+"&akhir="+akhir).load();
        }

        $('#daterangepick').daterangepicker({
            startDate: start,
            endDate: end,
            ranges: {
            'Today': [moment(), moment()],
            'Yesterday': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
            'Last 7 Days': [moment().subtract(6, 'days'), moment()],
            'Last 30 Days': [moment().subtract(29, 'days'), moment()],
            'This Month': [moment().startOf('month'), moment().endOf('month')],
            'Last Month': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
            }
        },cb);
        cb(start, end);
    });
    var table = $("#example1").DataTable({
        responsive: true,
        dom: 'Bfrtip',
        buttons: [{
                text: 'Hit pengiriman Baru',
                action: function(e, dt, node, config) {
                    $("#modal-xl").modal();
                    replaceoption();
                }
            },
            'copy', 'csv', 'print'
        ],
        "oLanguage": {
                "sSearch": "Pencarian",
                "sLengthMenu": "Tampilkan _MENU_ data per halaman",
                "sLoadingRecords": "Mohon tunggu - sedang mengambil data dari server...",
                "sEmptyTable": "Belum ada data...",
                "sInfoEmpty": "Menampilkan 0 - 0 dari 0 data",
                "sInfo": "Menampilkan _START_ - _END_ dari _TOTAL_ data",
                "sInfoFiltered":   "(Dicari dari _MAX_ total data)",
                "sZeroRecords":    "Data yang dicari tidak ada yang sesuai",
                "oPaginate": {                       
                        "sNext": '<i class="fa fa-angle-right" ></i>',
                        "sPrevious": '<i class="fa fa-angle-left"" ></i>'
                },
            },
            ajax:"{{route('datahitpengiriman')}}",
            columns: [
                { data: "id" },
                { data: "createdfrom" },
                { data: "nomor" },
                { data: "tanggal" },
                { data: "pedagang" },
                { data: "notruk" },
                { data: "item" },
                { data: "colly" },
                { data: "qty" },
                { data: "date" },
                { data: "id" }
            ],
            "order": [[ 0, "desc" ]],
            scrollX: true,
            scrollCollapse: true,
            columnDefs: [{
                    targets: -1,
                    title: 'Opsi',
                    orderable: false,
                    width: '170px',
                    render: function(data, type, full, meta) {
                        var tgl="'"+full.date+"'";
                        return '\
                            <a href="javascript:;" class="btn btn-sm btn-clean btn-icon mr-2 editbtn" title="Edit" onclick="edit('+data+')">\
                                <span class="svg-icon svg-icon-md">\
                                    <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1">\
                                        <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">\
                                            <rect x="0" y="0" width="24" height="24"/>\
                                            <path d="M8,17.9148182 L8,5.96685884 C8,5.56391781 8.16211443,5.17792052 8.44982609,4.89581508 L10.965708,2.42895648 C11.5426798,1.86322723 12.4640974,1.85620921 13.0496196,2.41308426 L15.5337377,4.77566479 C15.8314604,5.0588212 16,5.45170806 16,5.86258077 L16,17.9148182 C16,18.7432453 15.3284271,19.4148182 14.5,19.4148182 L9.5,19.4148182 C8.67157288,19.4148182 8,18.7432453 8,17.9148182 Z" fill="#000000" fill-rule="nonzero"\ transform="translate(12.000000, 10.707409) rotate(-135.000000) translate(-12.000000, -10.707409) "/>\
                                            <rect fill="#000000" opacity="0.3" x="5" y="20" width="15" height="2" rx="1"/>\
                                        </g>\
                                    </svg>\
                                </span>\
                            </a>\
                            <a href="javascript:;" class="btn btn-sm btn-clean btn-icon hapus" title="Hapus" onclick="hapus('+data+')" data-toggle="modal" data-target="#"modal-hapus">\
                                <span class="svg-icon svg-icon-md">\
                                    <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1">\
                                        <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">\
                                            <rect x="0" y="0" width="24" height="24"/>\
                                            <path d="M6,8 L6,20.5 C6,21.3284271 6.67157288,22 7.5,22 L16.5,22 C17.3284271,22 18,21.3284271 18,20.5 L18,8 L6,8 Z" fill="#000000" fill-rule="nonzero"/>\
                                            <path d="M14,4.5 L14,4 C14,3.44771525 13.5522847,3 13,3 L11,3 C10.4477153,3 10,3.44771525 10,4 L10,4.5 L5.5,4.5 C5.22385763,4.5 5,4.72385763 5,5 L5,5.5 C5,5.77614237 5.22385763,6 5.5,6 L18.5,6 C18.7761424,6 19,5.77614237 19,5.5 L19,5 C19,4.72385763 18.7761424,4.5 18.5,4.5 L14,4.5 Z" fill="#000000" opacity="0.3"/>\
                                        </g>\
                                    </svg>\
                                </span>\
                            </a>\
                        ';
                    },
                },
                {
                    targets: 0,
                    render: function(data, type, full, meta) {
                        return meta.row + 1;
                    },
                }
            ],
    });

    function edit(id){
        $.ajax({
            url: "{{route('hitspengirimandetail')}}",
            cache: false,
            dataType: 'json',
            data: {id:id},
            success: function(data){
                $("#modal-edit").modal();
                document.getElementById('idedit').value = id;
                document.getElementById('createdfromedit').value = data['createdfrom'];
                document.getElementById('nomoredit').value = data['nomor'];
                document.getElementById('tanggaledit').value = data['tanggal'];
                document.getElementById('penimbangedit').value = data['penimbang'];
                document.getElementById('notrukedit').value = data['notruk'];
                document.getElementById('itemedit').value = data['item'];
                document.getElementById('collyedit').value = data['colly'];
                document.getElementById('qtyedit').value = data['qty'];
                document.getElementById('unitedit').value = data['units'];
                document.getElementById('serialedit').value = data['serial'];
                document.getElementById('locationedit').value = data['location'];
                document.getElementById('pedagangedit').value = data['pedagang'];
                var fulfil=document.getElementById('nofulfillmentedit');
                if(data['createdfrom']=="Transfer Order"){
                    document.getElementById('fulfilmentedit').value='';
                    document.getElementById('fulfilmentedit').style.display='block';
                } else {
                    document.getElementById('fulfilmentedit').style.display='none';
                }
                if(fulfil){
                    document.getElementById('nofulfillmentedit').value = data['noitemfulfillment'];
                }
            }
        });
    }

    function hapus(hapus) {
        $('#hapusid').val(hapus);
        $("#modal-hapus").modal();
    }

    function replaceoption(){
        $.ajax({
                type: 'GET',
                url: '{{route("replacehitspengiriman")}}',
                success: function (data) {
                    var datas = JSON.parse(data);
                    var op='<option>Pilih pengiriman</option>';
                    for(var i=0;i<datas.length;i++){
                        var op = op + '<option value="'+datas[i].id+'" pedagang="'+datas[i].pedagang+'" nopol="'+datas[i].notruk+'" item="'+datas[i].item+'" units="'+datas[i].units+'" colly="'+datas[i].colly+'" netto="'+datas[i].netto+'">'+datas[i].tanggal+' - '+datas[i].pedagang+' - '+datas[i].notruk+' - '+datas[i].netto+' </option>';
                    }
                    $("#idasal").empty().append(op);
                }
            });
    }
    
    $(function() {  
        $('#savenewdata').on('submit', function (e) {
            e.preventDefault();
            var uri=$('#savenewdata').attr('action');
            $.ajax({
                type: 'POST',
                url: uri,
                data: $('#savenewdata').serialize(),
                success: function (data) {
                    var started = moment();
                    var ended = moment();
                    document.getElementById("savenewdata").reset();
                    $('#modal-xl').modal('hide');
                    $('#daterangepick span').html(started.format('MMMM DD, YYYY') + ' - ' + ended.format('MMMM DD, YYYY'));
                    table.ajax.url("{{route('datahitpengiriman')}}").load();
                    $(document).Toasts('create', {
                        icon: 'fas fa-check',
                        class: 'bg-success m-1',
                        autohide: true,
                        delay: 5000,
                        title: 'Informasi',
                        body: data
                    });
                    replaceoption();
                },
                error:function (a) {
                    $(document).Toasts('create', {
                        icon: 'fas fa-exclamation-triangle',
			            class: 'bg-danger m-1',
                        autohide: true,
                        delay: 5000,
                        title: 'Informasi',
                        body: '"Tidak dapat terhubung dengan server. Mohon coba lagi."'
                    })
                }
            });
        });
        $('#editdata').on('submit', function (e) {
            e.preventDefault();
            var uri=$('#editdata').attr('action');
            $.ajax({
                type: 'POST',
                url: uri,
                data: $('#editdata').serialize(),
                success: function (data) {
                    var started = moment();
                    var ended = moment();
                    document.getElementById("editdata").reset();
                    $('#modal-edit').modal('hide');
                    $('#daterangepick span').html(started.format('MMMM DD, YYYY') + ' - ' + ended.format('MMMM DD, YYYY'));
                    table.ajax.url("{{route('datahitpengiriman')}}").load();
                    $(document).Toasts('create', {
                        icon: 'fas fa-check',
                        class: 'bg-success m-1',
                        autohide: true,
                        delay: 5000,
                        title: 'Informasi',
                        body: data
                    })
                },
                error:function (a) {
                    $(document).Toasts('create', {
                        icon: 'fas fa-exclamation-triangle',
			            class: 'bg-danger m-1',
                        autohide: true,
                        delay: 5000,
                        title: 'Informasi',
                        body: '"Tidak dapat terhubung dengan server. Mohon coba lagi."'
                    })
                }
            });
        });
        $('#hapusdata').on('submit', function (e) {
            e.preventDefault();
            var uri=$('#hapusdata').attr('action');
            $.ajax({
                type: 'POST',
                url: uri,
                data: $('#hapusdata').serialize(),
                success: function (data) {
                    document.getElementById("hapusdata").reset();
                    $('#modal-hapus').modal('hide');
                    table.ajax.url("{{route('datahitpengiriman')}}").load();
                    $(document).Toasts('create', {
                        icon: 'fas fa-check',
                        class: 'bg-success m-1',
                        autohide: true,
                        delay: 5000,
                        title: 'Informasi',
                        body: data
                    });
                    replaceoption();
                },
                error:function (a) {
                    $(document).Toasts('create', {
                        icon: 'fas fa-exclamation-triangle',
			            class: 'bg-danger m-1',
                        autohide: true,
                        delay: 5000,
                        title: 'Informasi',
                        body: '"Tidak dapat terhubung dengan server. Mohon coba lagi."'
                    })
                }
            });
        });
    });
    
    function showfulfillment(name){
        if(name=="Transfer Order"){
            document.getElementById('fulfilinput').value='';
            document.getElementById('fulfilment').style.display='block';
        } else {
            document.getElementById('fulfilment').style.display='none';
        }
    }

    function showfulfillmentedit(name){
        if(name=="Transfer Order"){
            document.getElementById('fulfilmentedit').value='';
            document.getElementById('fulfilmentedit').style.display='block';
        } else {
            document.getElementById('fulfilmentedit').style.display='none';
        }
    }

    function setvalue(){ 
        var colly = parseInt(document.getElementById("idasal").options[document.getElementById("idasal").selectedIndex].getAttribute('colly')); 
        var netto = parseInt(document.getElementById("idasal").options[document.getElementById("idasal").selectedIndex].getAttribute('netto')); 
        var pedagang = document.getElementById("idasal").options[document.getElementById("idasal").selectedIndex].getAttribute('pedagang'); 
        var nopol = document.getElementById("idasal").options[document.getElementById("idasal").selectedIndex].getAttribute('nopol'); 
        var item = document.getElementById("idasal").options[document.getElementById("idasal").selectedIndex].getAttribute('item'); 
        var units = document.getElementById("idasal").options[document.getElementById("idasal").selectedIndex].getAttribute('units'); 
        document.getElementById('pedagang').value=pedagang;
        document.getElementById('nopol').value=nopol;
        document.getElementById('item').value=item;
        document.getElementById('units').value=units;
        $("#colly").attr({
        "max" : colly,
        "min" : 1
        });
        $("#qty").attr({
        "max" : netto,
        "min" : 1
        });
    }

    function setvalueedit(){
        var bruto = document.getElementById('brutoedit').value;
        var colly = document.getElementById('collyedit').value;
        var collyraw = 0;
        var beratraw = 0;
        var jml = 0;
        var package = document.getElementsByClassName('kodepackageedit');
        var jmlpackage = document.getElementsByClassName('jumlahpackageedit');
        for(var a=0;a<package.length;a++){
            var b = parseFloat(package[a].options[package[a].selectedIndex].getAttribute('berat'));
            beratraw=beratraw+b;
            if(jmlpackage[a].value!=null){
                jml = jml + parseFloat(jmlpackage[a].value);
            }
        }
        // if(jml!==parseInt(colly)){
        //     document.getElementById("simpantbnedit").style.display='none';
        //     alert("Jumlah Package harus sama dengan jumlah colly. Mohon untuk tidak mengisi sejumlah colly.");
        // } else {
            var tara = beratraw * jml;
            var netto = bruto-tara;
            document.getElementById('brutoedit').value=parseFloat(bruto);
            document.getElementById('taraedit').value=tara;
            document.getElementById('nettoedit').value=netto.toFixed(2);
            document.getElementById("simpantbnedit").style.display='block';
        // }
    }
    
    function formatDate(date) {
        var d = new Date(date),
            month = '' + (d.getMonth() + 1),
            day = '' + d.getDate(),
            year = d.getFullYear();

        if (month.length < 2) 
            month = '0' + month;
        if (day.length < 2) 
            day = '0' + day;

        return [year, month, day].join('-');
    }

</script>
@stop