@extends('adminlte::page')

@section('title', 'Timbangan Baru')

@section('content_header')
<h1>Timbangan Baru</h1>
@stop

@section('content')
<div class="row">
    <div class="col-xs-12 col-md-6">
        <div class="card">
            <div class="card-body">
                <div class="row">
                    <div class="col-xs-12 col-md-6 mb-2">
                        <label>Pedagang</label>
                        <input type="text" name="pedagang" id="pedagang" class="form-control" required>
                    </div>
                    <div class="col-xs-12 col-md-6 mb-2">
                    <label>No. Truk</label>
                        <input type="text" name="notruk" id="notruk" class="form-control" required>
                    </div>
                    <div class="col-xs-12 col-md-6 mb-2">
                        <label>Type</label>
                        <select name="tipe" id="tipe" class="form-control" required>
                            <option>Pilih Tipe</option>
                            <option value="Penerimaan">Penerimaan</option>
                            <option value="Pengiriman">Pengiriman</option>
                        </select>
                    </div>
                    <div class="col-xs-12 col-md-6 mb-2">
                        <label>Item</label>
                        <select name="item" id="item" class="form-control" onchange="getsample()" required>
                            <option>Pilih Item</option>
                            @foreach(\App\Models\Item::get() as $data)
                            <option value="{{$data->id}}" data-sample="{{$data->sample}}">{{$data->namaitem}}</option>
                            @endforeach
                        </select>
                    </div>
                    <div class="col-xs-12 col-md-12 mb-2 hiddenawal" style="display:none;">
                        <h2><b>Berat</b></h2>
                        <input type="number" class="form-control mb-2" id="berat" style="font-size:105px; min-height:110px;" autofocus>
                    </div>
                    <div class="col-xs-12 col-md-4 mb-2 hiddenawal" style="display:none;">
                        <button class="btn btn-primary btn-block mb-2" onclick="add()"><i class="fa fa-plus"></i> Tambah Data</button>
                    </div>
                    <div class="col-xs-12 col-md-4 mb-2 hiddenawal" style="display:none;">
                        <button class="btn btn-primary btn-block mb-2" onclick="bulkstore()"><i class="fa fa-save"></i> Simpan Data</button>
                    </div>
                    <div class="col-xs-12 col-md-4 mb-2 hiddenawal" style="display:none;">
                        <button class="btn btn-danger btn-block mb-2" onclick = "removeall()"><i class="fa fa-trash"></i> Hapus Semua Data</button>
                    </div>
                    <div class="col-xs-12 col-md-12 mb-2 hiddenawal" style="display:none;">
                        <hr style="border-top:1px dashed #cccccc;">
                    </div>
                    <div class="col-xs-12 col-md-12 hiddenawal" style="display:none;">
                        <h2><b>Bruto</b></h2>
                        <input type="number" class="form-control mb-2" id="bruto" style="font-size:105px; min-height:110px;" readonly value=0>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="col-xs-12 col-md-6">
        <div class="card">
            <!-- /.card-header -->
            <div class="card-body">
                <table id="example1" class="table table-bordered table-striped dataTable" width="100%" style="font-size:14px;">
                    <thead>
                        <tr role="row">
                            <th width="5%">#</th>
                            <th>Berat</th>
                            <th>Tanggal</th>
                            <th>Hapus</th>
                        </tr>
                    </thead>
                </table>
            </div>
        </div>
        <!-- /.card-body -->
    </div>
</div>
</div>
<div id="overlay"></div>
<div class="modal fade" id="modal-hapus">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title">Hapus Data</h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
                <div class="modal-body">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="form-group">
                                Apakah anda yakin ingin menghapus data?
                                <input type="hidden" class="form-control" id="ids">
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Tidak</button>
                    <button type="button" class="btn btn-danger" onclick="hapusrecord()">Ya</button>
                </div>
        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>
@stop

@section('css')
<link rel="stylesheet" href="{{asset('vendor/daterangepicker/daterangepicker.css')}}">
<style>
    #overlay {
        position: fixed; /* Sit on top of the page content */
        display: none; /* Hidden by default */
        width: 100%; /* Full width (cover the whole page) */
        height: 100%; /* Full height (cover the whole page) */
        top: 0;
        left: 0;
        right: 0;
        bottom: 0;
        background-color: rgba(0,0,0,0.5); /* Black background with opacity */
        z-index: 10000; /* Specify a stack order in case you're using a different order for other elements */
        cursor: pointer; /* Add a pointer on hover */
        background-image: url("{{asset('vendor/adminlte/dist/img/loading-buffering.gif')}}");
        background-repeat: no-repeat;
        background-attachment: fixed;
        background-position: center;
        background-size: 100px 100px;
    }
</style>
@stop

@section('js')
<script src="{{asset('vendor/datatables/js/jquery.dataTables.min.js')}}"></script>
<script src="{{asset('vendor/datatables/js/dataTables.bootstrap4.min.js')}}"></script>
<script src="{{asset('vendor/daterangepicker/moment.min.js')}}"></script>
<script src="{{asset('vendor/daterangepicker/daterangepicker.js')}}"></script>
<script>
    var table = $("#example1").DataTable({
        responsive: true,
        "oLanguage": {
                "sSearch": "Pencarian",
                "sLengthMenu": "Tampilkan _MENU_",
                "sLoadingRecords": "Mohon tunggu - sedang mengambil data dari server...",
                "sEmptyTable": "Belum ada data",
                "sInfoEmpty": "Menampilkan 0 - 0 dari 0 data",
                "sInfo": "Menampilkan _START_ - _END_ dari _TOTAL_ data",
                "sInfoFiltered":   "(Dicari dari _MAX_ total data)",
                "sZeroRecords":    "Data yang dicari tidak ada yang sesuai",
                "oPaginate": {                       
                        "sNext": '<i class="fa fa-angle-right" ></i>',
                        "sPrevious": '<i class="fa fa-angle-left"" ></i>'
                },
            },
            "order": [[ 0, "desc" ]],
            columnDefs: [{
                    targets: 1,
                    title: 'Berat',
                    render: function(data, type, full, meta) {
                        return data+' Kg';
                    },
                },
                {
                    targets: 0,
                    render: function(data, type, full, meta) {
                        return meta.row + 1;
                    },
                },
                {
                    targets: -1,
                    width:'40px',
                    render: function(data, type, full, meta) {
                        return '<button class="btn btn-danger btn-sm btn-icon" onclick="remove('+data+')"><i class="fa fa-times"></i></button>';
                    },
                }
            ],
    });
</script>
<script type = "text/javascript">
    //prefixes of implementation that we want to test
    window.indexedDB = window.indexedDB || window.mozIndexedDB || 
    window.webkitIndexedDB || window.msIndexedDB;
    
    //prefixes of window.IDB objects
    window.IDBTransaction = window.IDBTransaction || 
    window.webkitIDBTransaction || window.msIDBTransaction;
    window.IDBKeyRange = window.IDBKeyRange || window.webkitIDBKeyRange || 
    window.msIDBKeyRange
    
    if (!window.indexedDB) {
    window.alert("Your browser doesn't support a stable version of IndexedDB.")
    }
    
    // const employeeData = [
    // { berat: 20, datetime: '2021-11-13 10:00'},
    // { berat: 23, datetime: '2021-11-13 11:00'},
    // ];
    var db;
    var idkeys=0;
    var request = window.indexedDB.open("timbangan", 1);
    var nomor=1;
    request.onerror = function(event) {
    console.log("error: ");
    };
    
    request.onsuccess = function(event) {
    db = request.result;
    console.log("success: "+ db);
    };
    
    request.onupgradeneeded = function(event) {
        var db = event.target.result;
        var objectStore = db.createObjectStore("berat", {keyPath: "id",autoIncrement: true});
        // for (var i in employeeData) {
        //     objectStore.add(employeeData[i]);
        // }
    }
    var sample=1;
    var counter=1;
    function getsample(){
        var e = document.getElementById("item");
        //var sample = e.options[e.selectedIndex].attr('data-sample');
        var s = e.options[e.selectedIndex].getAttribute('data-sample');
        sample=parseInt(s);
        var h = document.getElementsByClassName('hiddenawal');
        for(var a=0; a<h.length;a++){
            h[a].style.display="block";
        }
    }

    function read() {
        var objectStore =  db.transaction("berat").objectStore("berat");
        console.log(objectStore);
        var request = objectStore.get(1);
        
        request.onerror = function(event) {
            alert("Unable to retrieve daa from database!");
        };
        
        request.onsuccess = function(event) {
            // Do something with the request.result!
            if(request.result) {
                alert("Name for berat is " + request.result.berat + ", Tanggal: " + request.result.datetime);
            } else {
                alert("Data couldn't be found in your database!");
            }
        };
    }
    
    function readAll() {
        nomor=1;
        var bruto=0;
        var objectStore = db.transaction("berat").objectStore("berat");
        table.clear();
        objectStore.openCursor().onsuccess = function(event) {
            var cursor = event.target.result;
            if (cursor) {
                idtemp = cursor.value.id+1;
                if(idtemp>idkeys){
                    idkeys=cursor.value.id+1;
                }
                //alert("Name for id " + cursor.key + " is " + cursor.value.berat + ", Tanggal: " + cursor.value.datetime);
                table.row.add( [cursor.key,cursor.value.berat,cursor.value.datetime,cursor.value.id] ).draw();
                bruto = parseFloat(bruto) + parseFloat(cursor.value.berat);
                cursor.continue();
                nomor++;
            }
            document.getElementById('bruto').value=bruto.toFixed(2);
            if(bruto.toFixed(2) == '0.00'){
                table.clear().draw();
            }
        };
    }
    
    function add() {
        var id=idkeys;
        var bruto = document.getElementById('bruto').value;
        var weight = document.getElementById('berat').value;
        var currentdate = new Date(); 
        var datetimes = currentdate.getFullYear() + "-"
                + (currentdate.getMonth()+1)  + "-" 
                + currentdate.getDate() + " "  
                + currentdate.getHours() + ":"  
                + currentdate.getMinutes() + ":" 
                + currentdate.getSeconds();
        var request = db.transaction(["berat"], "readwrite")
        .objectStore("berat")
        .add({ berat: weight, datetime: datetimes });
        request.onsuccess = function(event) {
            document.getElementById('berat').value='';
            document.getElementById('berat').focus();
            bruto = parseFloat(bruto) + parseFloat(weight);
            document.getElementById('bruto').value=bruto.toFixed(2);
            readAll();
        };
        
        request.onerror = function(event) {
            console.log("Unable to add data\r\Data is aready exist in your database! ");
        }
        if(counter==sample){
            playSound();
            counter=0;
        }
        counter++;
        cetak(nomor,weight);
        nomor++;
        bacatimbangan();
    }

    function cetak(nomor,weight){
        var pedagang = document.getElementById('pedagang').value;
        var notruk = document.getElementById('notruk').value;
        var item = document.getElementById('item').value;
        var url = "{{route('cetaksatuan')}}?nomor="+nomor+"&berat="+weight+"&pedagang="+pedagang+"&notruk="+notruk+"&item="+item;
        window.open(url, '_blank','"width=100,height=100"');
    }

    function cetakacum(){
        var bruto = document.getElementById('bruto').value;
        var url = "{{route('cetakacum')}}?acum="+bruto;
        window.open(url, '_blank','"width=100,height=100"');
    }

    function playSound() {
        let audioData = "{{asset('notification.wav')}}";
        let audioEl = document.createElement('audio');
        audioEl.src = audioData;
        let audioBtn = document.createElement('button');
        audioBtn.addEventListener('click', () => audioEl.play(), false);
        let event = new Event('click');
        audioBtn.dispatchEvent(event);
    }
    
    function remove(ids) {
        document.getElementById('ids').value=ids;
        $('#modal-hapus').modal('show');
    }

    function hapusrecord(){
        var id = document.getElementById('ids').value;
        var ids = parseInt(id);
        var request = db.transaction(["berat"], "readwrite").objectStore("berat").delete(ids);
        request.onsuccess = function(event) {
            $('#modal-hapus').modal('hide');
        };
        readAll();
        nomor--;
    }
    function removeall() {
        indexedDB.deleteDatabase('timbangan');
        nomor=1;
        location.reload();
    }

    setTimeout( function() { readAll(); }, 1000);

    // Get the input field
    var input = document.getElementById("berat");

    // Execute a function when the user releases a key on the keyboard
    input.addEventListener("keyup", function(event) {
        // Number 13 is the "Enter" key on the keyboard
        if (event.keyCode === 13) {
            // Cancel the default action, if needed
            event.preventDefault();
            // Trigger the button element with a click
            add();
        }
    });
    function bulkstore(){
        document.getElementById('overlay').style.display='block';
        var objectStore = db.transaction("berat","readonly").objectStore("berat");
        var request = objectStore.openCursor(); 
        request.onsuccess = function() {
            var cursor = request.result;
            if (cursor) {
                var data=[cursor.key,parseFloat(cursor.value.berat),cursor.value.datetime];
                collectdata(data);
                cursor.continue();
            }
        };
        setTimeout( function() { calldata(); cetakacum();}, 5000);
    }

    var datas=[];
    function collectdata(array){
        return datas.push(array);
    }
    function calldata(){
        senddata(datas);
    }
    function senddata(datas){
        var uri="{{route('bulkstoretimbangan')}}";
        var pedagang = document.getElementById('pedagang').value;
        var notruk = document.getElementById('notruk').value;
        var item = document.getElementById('item').value;
        var tipe = document.getElementById('tipe').value;
        if(datas!==null && datas!==''){
            $.ajax({
                type: 'POST',
                url: uri,
                data: {data:datas,tipe:tipe,pedagang:pedagang,notruk:notruk,item:item},
                success: function (data) {
                    setTimeout( function() { location.href = "{{route('timbangan')}}"; }, 1000);
                    document.getElementById('overlay').style.display='none';
                    indexedDB.deleteDatabase('timbangan');
                    table.clear();
                    document.getElementById('bruto').value=0,00;
                    $(document).Toasts('create', {
                        icon: 'fas fa-check',
                        class: 'bg-success m-1',
                        autohide: true,
                        delay: 5000,
                        title: 'Informasi',
                        body: data
                    })
                },
                error:function (a) {
                    document.getElementById('overlay').style.display='none';
                    $(document).Toasts('create', {
                        icon: 'fas fa-exclamation-triangle',
			            class: 'bg-danger m-1',
                        autohide: true,
                        delay: 5000,
                        title: 'Informasi',
                        body: '"Tidak dapat terhubung dengan server. Mohon coba lagi."'
                    })
                }
            });
        }
    }

    function bacatimbangan() {
		fetch("http://localhost:8999/httplistener/")
		.then(response => response.json())
		.then(function(data1) {
		//   console.log(data1);
		  document.getElementById('berat').value=data1.Berat;
		})
		.catch(function(error) {
		  // If there is any error you will catch them here
		  document.getElementById('berat').value=0;
		});
	};

    setInterval(bacatimbangan(), 1000);
</script>
@stop