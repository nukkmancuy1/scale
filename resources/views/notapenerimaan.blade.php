@extends('adminlte::page')

@section('title', 'Nota Penerimaan')

@section('content_header')
<h1>Nota Penerimaan</h1>
@stop

@section('content')
<div class="row">
    <div class="col-12">
        <div class="card">
            <!-- /.card-header -->
            <div class="card-header">
                <div class="row">
                    <div class="col-xs-12 col-md-8">&nbsp;</div>
                    <div class="col-xs-12 col-md-1 pt-2">
                        Periode : 
                    </div>
                    <div class="col-xs-12 col-md-3">
                        <input type="text" class="form-control" id="daterangepick">
                    </div>
                </div>
            </div>
            <div class="card-body">
                <table id="example1" class="table table-bordered table-striped dataTable" width="100%">
                    <thead>
                        <tr role="row">
                            <th width="5%">#</th>
                            <th>Tanggal</th>
                            <th>Pedagang</th>
                            <th>No Truk</th>
                            <th>Item</th>
                            <th>Bruto</th>
                            <th>Netto</th>
                            <th>Dibuat Tanggal</th>
                            <th>Pilihan</th>
                        </tr>
                    </thead>
                </table>
            </div>
        </div>
        <!-- /.card-body -->
    </div>
</div>
</div>

<div class="modal fade" id="modal-xl">
    <div class="modal-dialog modal-xl">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title">Tambahkan nota penerimaan baru</h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <form method="POST" action="{{ route('notapenerimaanbaru') }}" id="savenewdata">
                <div class="modal-body">
                    <div class="row">
                        <div class="col-xs-12 col-md-3 mb-2">
                            <div class="form-group">
                                <label>Kode Timbangan</label>
                                <select name="kodetimbangan" id="kodetimbangan" class="form-control" required onchange="setvalue()">
                                    <option>Pilih Kode Timbangan</option>
                                    @foreach(DB::select('select timbangan.kodetimbangan, count(*) as colly,  sum(quantitydetails) as bruto, 
                                            datatimbangan.pedagang, datatimbangan.nopol, datatimbangan.tipe, item.namaitem, datatimbangan.iditem
                                            from timbangan 
                                            left join datatimbangan on timbangan.kodetimbangan=datatimbangan.kodetimbangan 
                                            left join item on datatimbangan.iditem=item.id
                                            where datatimbangan.tipe="Penerimaan" 
                                            and timbangan.kodetimbangan not in (select kodetimbangan from notapenerimaan where deleted_at is null) 
                                            and timbangan.deleted_at is null 
                                            group by timbangan.kodetimbangan') as $datatimbangan)
                                    <option value="{{$datatimbangan->kodetimbangan}}"
                                            bruto="{{$datatimbangan->bruto}}" 
                                            pedagang="{{$datatimbangan->pedagang}}" 
                                            nopol="{{$datatimbangan->nopol}}" 
                                            item="{{$datatimbangan->namaitem}}"
                                            colly="{{$datatimbangan->colly}}">
                                            {{$datatimbangan->kodetimbangan}}
                                    </option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                        <div class="col-xs-12 col-md-3 mb-2">
                            <div class="form-group">
                                <label>Tanggal</label>
                                <input type="date" class="form-control datepicker" name="tanggal" value="{{date('Y-m-d')}}" required>
                            </div>
                        </div>
                        <div class="col-xs-12 col-md-3 mb-2">
                            <div class="form-group">
                                <label>Unit</label>
                                <select name="unit" class="form-control" required>
                                    <option>Pilih Unit</option>
                                    <option value="KG">KG</option>
                                </select>
                            </div>
                        </div>
                        <div class="col-xs-12 col-md-3 mb-2">
                            <div class="form-group">
                                <label>Pedagang</label>
                                <input type="text" class="form-control" name="pedagang" id="pedagang" readonly required>
                            </div>
                        </div>
                        <div class="col-xs-12 col-md-3 mb-2">
                            <div class="form-group">
                                <label>Nomor Truk</label>
                                <input type="text" class="form-control" name="notruk" id="nopol" readonly required>
                            </div>
                        </div>
                        <div class="col-xs-12 col-md-3 mb-2">
                            <div class="form-group">
                                <label>Item</label>
                                <input type="text" class="form-control" name="item" id="item" readonly required>
                            </div>
                        </div>
                        <div class="col-xs-12 col-md-3 mb-2">
                            <div class="form-group">
                                <label>Colly</label>
                                <input type="number" class="form-control" name="colly" id="colly" required>
                            </div>
                        </div>
                        <div class="col-xs-12 col-md-3 mb-2">
                            <div class="form-group">
                                <label>Bruto</label>
                                <input type="text" class="form-control" name="bruto" id="bruto" readonly required>
                            </div>
                        </div>
                        <div class="col-xs-12 col-md-3 mb-2">
                            <div class="form-group">
                                <label>Tara</label>
                                <input type="text" class="form-control" name="tara" id="tara" readonly required>
                            </div>
                        </div>
                        <div class="col-xs-12 col-md-3 mb-2">
                            <div class="form-group">
                                <label>Netto</label>
                                <input type="text" class="form-control" name="netto" id="netto" readonly required>
                            </div>
                        </div>
                        <div class="col-xs-12 col-md-3 mb-2">
                            <div class="form-group">
                                <label>Location</label>
                                <input type="text" class="form-control" name="location" value="{{auth()->user()->kodegudang}}" readonly required>
                            </div>
                        </div>
                        <div class="col-xs-12 col-md-3 mb-2">
                            <div class="form-group">
                                <label>Penimbang</label>
                                @csrf
                                <input type="text" class="form-control" name="penimbang" readonly value="{{auth()->user()->fullname}}" required>
                            </div>
                        </div>
                        <div class="col-xs-12 col-md-12 mb-2">
                            <table class="table table-bordered" id="tablepackage">
                                <tr>
                                    <th width="50%">Kode Package</th>
                                    <th width="45%">Jumlah</th>
                                    <th width="5%">
                                        <button type="button" class="btn btn-primary" onclick="addrow()"><i class="fa fa-plus"></i></button>
                                    </th>
                                </tr>
                                <tbody id="bodytr">
                                <tr>
                                    <td>
                                        <select name="kodepackage[]" class="form-control kodepackage" onchange="setvalue()" required>
                                            <option>Pilih Kode Package</option>
                                            @foreach(\App\Models\Package::get() as $data)
                                                <option value="{{$data->id}}" berat="{{$data->beratpackage}}">{{$data->namapackage}}</option>
                                            @endforeach
                                        </select>
                                    </td>
                                    <td>
                                        <input class="form-control jumlahpackage" name="jumlahpackage[]" onchange="setvalue()" value=1 required>
                                    </td>
                                    <td>
                                        <button type="button" class="btn btn-danger" onclick="deletetr(this)"><i class="fa fa-times"></i></button>
                                    </td>
                                </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
                <div class="modal-footer justify-content-between">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    <button type="submit" class="btn btn-primary" id="simpantbn" style="display:none;">Save</button>
                </div>
            </form>
        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>
<div class="modal fade" id="modal-edit">
    <div class="modal-dialog modal-xl">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title">Ubah data nota penerimaan</h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <form method="POST" action="{{ route('editnotapenerimaan') }}" id="editdata">
                <div class="modal-body">
                    <div class="row">
                        <div class="col-xas-12 col-md-3 mb-2">
                            <div class="form-group">
                                <label>Tanggal</label>
                                <input type="hidden" class="form-control" id="idedit" name="id">
                                <input type="date" class="form-control datepicker" name="tanggal" id="tanggaledit" required>
                            </div>
                        </div>                        
                        <div class="col-xas-12 col-md-3 mb-2">
                            <div class="form-group">
                                <label>Pedagang</label>
                                <input type="text" class="form-control" name="pedagang" id="pedagangedit" required readonly>
                            </div>
                        </div>
                        <div class="col-xas-12 col-md-3 mb-2">
                            <div class="form-group">
                                <label>Nomor Truk</label>
                                <input type="text" class="form-control" name="notruk" id="notrukedit" required readonly>
                            </div>
                        </div>
                        <div class="col-xas-12 col-md-3 mb-2">
                            <div class="form-group">
                                <label>Item</label>
                                <input type="text" class="form-control" name="item" id="itemedit" required readonly>
                            </div>
                        </div>
                        <div class="col-xas-12 col-md-3 mb-2">
                            <div class="form-group">
                                <label>Colly</label>
                                <input type="number" class="form-control" name="colly" id="collyedit" required>
                            </div>
                        </div>
                        <div class="col-xas-12 col-md-3 mb-2">
                            <div class="form-group">
                                <label>Unit</label>
                                <select name="unit" class="form-control" id="unitedit" required>
                                    <option>Pilih Unit</option>
                                    <option value="KG">KG</option>
                                </select>
                            </div>
                        </div>
                        <div class="col-xas-12 col-md-3 mb-2">
                            <div class="form-group">
                                <label>Bruto</label>
                                <input type="text" class="form-control" name="bruto" id="brutoedit" readonly required>
                            </div>
                        </div>
                        <div class="col-xas-12 col-md-3 mb-2">
                            <div class="form-group">
                                <label>Tara</label>
                                <input type="text" class="form-control" name="tara" id="taraedit" readonly required>
                            </div>
                        </div>
                        <div class="col-xas-12 col-md-3 mb-2">
                            <div class="form-group">
                                <label>Netto</label>
                                <input type="text" class="form-control" name="netto" id="nettoedit" readonly required>
                            </div>
                        </div>
                        <div class="col-xas-12 col-md-3 mb-2">
                            <div class="form-group">
                                <label>Location</label>
                                <input type="text" class="form-control" name="location" id="locationedit" value="{{auth()->user()->kodegudang}}" readonly required>
                            </div>
                        </div>
                        <div class="col-xas-12 col-md-3 mb-2">
                            <div class="form-group">
                                <label>Penimbang</label>
                                @csrf
                                <input type="text" class="form-control" name="penimbang" id="penimbangedit" readonly value="{{auth()->user()->fullname}}" required>
                            </div>
                        </div>
                        <div class="col-xs-12 col-md-12 mb-2">
                            <table class="table table-bordered" id="tablepackageedit">
                                <tr>
                                    <th width="50%">Kode Package</th>
                                    <th width="45%">Jumlah</th>
                                    <th width="5%">
                                        <button type="button" class="btn btn-primary" onclick="addrowedit()"><i class="fa fa-plus"></i></button>
                                    </th>
                                </tr>
                                <tbody id="bodytredit">
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
                <div class="modal-footer justify-content-between">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    <button type="submit" class="btn btn-primary"  id="simpantbnedit">Save</button>
                </div>
            </form>
        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>
<div class="modal fade" id="modal-hapus">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title">Hapus Data</h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <form method="POST" action="{{ route('hapusnotapenerimaan') }}" id="hapusdata">
                <div class="modal-body">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="form-group">
                                Apakah anda yakin ingin menghapus data?
                                @csrf
                                <input type="hidden" class="form-control" name="id" id="hapusid">
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Tidak</button>
                    <button type="submit" class="btn btn-danger">Ya</button>
                </div>
            </form>
        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>
<!-- /.modal -->
<div class="modal fade" id="modal-cetak">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title">Cetak Data</h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="col-md-12">
                        <div class="form-group">
                            <label>Tanggal Cetak</label>
                            <input type="date" class="form-control" name="tglcetak" id="tglcetak">
                            <input type="hidden" class="form-control" name="idcetak" id="idcetak">
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Tidak</button>
                <button type="button" class="btn btn-danger" onclick="cetakya()">Ya</button>
            </div>
        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>
<!-- /.modal -->
@stop

@section('css')
<link rel="stylesheet" href="vendor/datatables/css/dataTables.bootstrap4.css">
<link rel="stylesheet" href="vendor/datatables-plugins/buttons/css/buttons.bootstrap4.min.css">
<link rel="stylesheet" href="vendor/daterangepicker/daterangepicker.css">
@stop

@section('js')
<script src="vendor/datatables/js/jquery.dataTables.min.js"></script>
<script src="vendor/datatables/js/dataTables.bootstrap4.min.js"></script>
<script src="vendor/datatables-plugins/buttons/js/buttons.bootstrap4.min.js"></script>
<script src="vendor/datatables-plugins/buttons/js/dataTables.buttons.min.js"></script>
<script src="vendor/datatables-plugins/buttons/js/buttons.colVis.min.js"></script>
<script src="vendor/datatables-plugins/buttons/js/buttons.flash.min.js"></script>
<script src="vendor/datatables-plugins/buttons/js/buttons.html5.min.js"></script>
<script src="vendor/datatables-plugins/buttons/js/buttons.print.min.js"></script>
<script src="vendor/daterangepicker/moment.min.js"></script>
<script src="vendor/daterangepicker/daterangepicker.js"></script>
<script>
    var start = moment();
    var end = moment();
    $(function() {
        function cb(started, ended) {
            $('#daterangepick span').html(started.format('MMMM D, YYYY') + ' - ' + ended.format('MMMM D, YYYY'));
            start=started;
            end=ended;
            var awal=start.format('YYYY-MM-D');
            var akhir=end.format('YYYY-MM-D');
            table.ajax.url("{{route('notapenerimaandata')}}?awal="+awal+"&akhir="+akhir).load();
        }

        $('#daterangepick').daterangepicker({
            startDate: start,
            endDate: end,
            ranges: {
            'Today': [moment(), moment()],
            'Yesterday': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
            'Last 7 Days': [moment().subtract(6, 'days'), moment()],
            'Last 30 Days': [moment().subtract(29, 'days'), moment()],
            'This Month': [moment().startOf('month'), moment().endOf('month')],
            'Last Month': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
            }
        },cb);
        cb(start, end);
    });
    var table = $("#example1").DataTable({
        responsive: true,
        dom: 'Bfrtip',
        buttons: [{
                text: 'Nota Penerimaan Baru',
                action: function(e, dt, node, config) {
                    $("#modal-xl").modal();
                }
            },
            'copy', 'csv', 'print'
        ],
        "oLanguage": {
                "sSearch": "Pencarian",
                "sLengthMenu": "Tampilkan _MENU_ data per halaman",
                "sLoadingRecords": "Mohon tunggu - sedang mengambil data dari server...",
                "sEmptyTable": "Belum ada data...",
                "sInfoEmpty": "Menampilkan 0 - 0 dari 0 data",
                "sInfo": "Menampilkan _START_ - _END_ dari _TOTAL_ data",
                "sInfoFiltered":   "(Dicari dari _MAX_ total data)",
                "sZeroRecords":    "Data yang dicari tidak ada yang sesuai",
                "oPaginate": {                       
                        "sNext": '<i class="fa fa-angle-right" ></i>',
                        "sPrevious": '<i class="fa fa-angle-left"" ></i>'
                },
            },
            ajax:"{{route('notapenerimaandata')}}",
            columns: [
                { data: "id" },
                { data: "tanggal" },
                { data: "pedagang" },
                { data: "notruk" },
                { data: "item" },
                { data: "bruto" },
                { data: "netto" },
                { data: "date" },
                { data: "id" }
            ],
            "order": [[ 0, "desc" ]],
            scrollX: true,
            scrollCollapse: true,
            columnDefs: [{
                    targets: -1,
                    title: 'Opsi',
                    orderable: false,
                    width: '170px',
                    render: function(data, type, full, meta) {
                        var tgl="'"+full.date+"'";
                        return '\
                            <a href="javascript:;" class="btn btn-sm btn-clean btn-icon mr-2 editbtn" title="Edit" onclick="edit('+data+')">\
                                <span class="svg-icon svg-icon-md">\
                                    <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1">\
                                        <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">\
                                            <rect x="0" y="0" width="24" height="24"/>\
                                            <path d="M8,17.9148182 L8,5.96685884 C8,5.56391781 8.16211443,5.17792052 8.44982609,4.89581508 L10.965708,2.42895648 C11.5426798,1.86322723 12.4640974,1.85620921 13.0496196,2.41308426 L15.5337377,4.77566479 C15.8314604,5.0588212 16,5.45170806 16,5.86258077 L16,17.9148182 C16,18.7432453 15.3284271,19.4148182 14.5,19.4148182 L9.5,19.4148182 C8.67157288,19.4148182 8,18.7432453 8,17.9148182 Z" fill="#000000" fill-rule="nonzero"\ transform="translate(12.000000, 10.707409) rotate(-135.000000) translate(-12.000000, -10.707409) "/>\
                                            <rect fill="#000000" opacity="0.3" x="5" y="20" width="15" height="2" rx="1"/>\
                                        </g>\
                                    </svg>\
                                </span>\
                            </a>\
                            <a href="javascript:;" target="_blank" class="btn btn-sm btn-clean btn-icon mr-2" title="Cetak" onclick="cetak('+data+','+tgl+')" data-toggle="modal" data-target="#"modal-hapus">\
                                <i class="fa fa-print"></i>\
                            </a>\
                            <a href="javascript:;" class="btn btn-sm btn-clean btn-icon hapus" title="Hapus" onclick="hapus('+data+')" data-toggle="modal" data-target="#"modal-hapus">\
                                <span class="svg-icon svg-icon-md">\
                                    <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1">\
                                        <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">\
                                            <rect x="0" y="0" width="24" height="24"/>\
                                            <path d="M6,8 L6,20.5 C6,21.3284271 6.67157288,22 7.5,22 L16.5,22 C17.3284271,22 18,21.3284271 18,20.5 L18,8 L6,8 Z" fill="#000000" fill-rule="nonzero"/>\
                                            <path d="M14,4.5 L14,4 C14,3.44771525 13.5522847,3 13,3 L11,3 C10.4477153,3 10,3.44771525 10,4 L10,4.5 L5.5,4.5 C5.22385763,4.5 5,4.72385763 5,5 L5,5.5 C5,5.77614237 5.22385763,6 5.5,6 L18.5,6 C18.7761424,6 19,5.77614237 19,5.5 L19,5 C19,4.72385763 18.7761424,4.5 18.5,4.5 L14,4.5 Z" fill="#000000" opacity="0.3"/>\
                                        </g>\
                                    </svg>\
                                </span>\
                            </a>\
                        ';
                    },
                },
                {
                    targets: 0,
                    render: function(data, type, full, meta) {
                        return meta.row + 1;
                    },
                }
            ],
    });

    function edit(id){
        $.ajax({
            url: "{{route('notapenerimaandetail')}}",
            cache: false,
            dataType: 'json',
            data: {id:id},
            success: function(data){
                $("#modal-edit").modal();
                document.getElementById('idedit').value = id;
                document.getElementById('tanggaledit').value = data['tanggal'];
                document.getElementById('penimbangedit').value = data['penimbang'];
                document.getElementById('notrukedit').value = data['notruk'];
                document.getElementById('itemedit').value = data['item'];
                document.getElementById('collyedit').value = data['colly'];
                document.getElementById('nettoedit').value = data['netto'];
                document.getElementById('unitedit').value = data['units'];
                document.getElementById('locationedit').value = data['location'];
                document.getElementById('pedagangedit').value = data['pedagang'];
                document.getElementById('brutoedit').value = data['bruto'];
                document.getElementById('taraedit').value = data['tara'];
                var fulfil=document.getElementById('nofulfillmentedit');
                var package = data['kodepackage'].split(";");
                $("#bodytredit").empty();
                for(var a=0;a<package.length-1;a++){
                    var item=package[a].split(":");
                    $('#bodytredit').append('<tr><td><select name="kodepackage[]" class="form-control kodepackageedit" onchange="setvalueedit()" id="item'+item[0]+item[1]+'" required><option>Pilih Kode Package</option>@foreach(\App\Models\Package::get() as $data)<option value="{{$data->id}}" berat="{{$data->beratpackage}}">{{$data->namapackage}}</option>@endforeach</select></td><td><input class="form-control jumlahpackageedit" name="jumlahpackage[]" onchange="setvalueedit()" value='+item[1]+' required></td><td><button type="button" class="btn btn-danger" onclick="deletetr(this)"><i class="fa fa-times"></i></button></td></tr>');
                    document.getElementById('item'+item[0]+item[1]).value=item[0];
                }
                if(data['createdfrom']=="Transfer Order"){
                    document.getElementById('fulfilmentedit').value='';
                    document.getElementById('fulfilmentedit').style.display='block';
                } else {
                    document.getElementById('fulfilmentedit').style.display='none';
                }
                if(fulfil){
                    document.getElementById('nofulfillmentedit').value = data['noitemfulfillment'];
                }
            }
        });
    }

    function hapus(hapus) {
        $('#hapusid').val(hapus);
        $("#modal-hapus").modal();
    }

    $(function() {  
        $('#savenewdata').on('submit', function (e) {
            e.preventDefault();
            var uri=$('#savenewdata').attr('action');
            $.ajax({
                type: 'POST',
                url: uri,
                data: $('#savenewdata').serialize(),
                success: function (data) {
                    document.getElementById("savenewdata").reset();
                    $('#modal-xl').modal('hide');
                    table.ajax.url("{{route('notapenerimaandata')}}").load();
                    $(document).Toasts('create', {
                        icon: 'fas fa-check',
                        class: 'bg-success m-1',
                        autohide: true,
                        delay: 5000,
                        title: 'Informasi',
                        body: data
                    })
                },
                error:function (a) {
                    $(document).Toasts('create', {
                        icon: 'fas fa-exclamation-triangle',
			            class: 'bg-danger m-1',
                        autohide: true,
                        delay: 5000,
                        title: 'Informasi',
                        body: '"Tidak dapat terhubung dengan server. Mohon coba lagi."'
                    })
                }
            });
        });
        $('#editdata').on('submit', function (e) {
            e.preventDefault();
            var uri=$('#editdata').attr('action');
            $.ajax({
                type: 'POST',
                url: uri,
                data: $('#editdata').serialize(),
                success: function (data) {
                    document.getElementById("editdata").reset();
                    $('#modal-edit').modal('hide');
                    table.ajax.url("{{route('notapenerimaandata')}}").load();
                    $(document).Toasts('create', {
                        icon: 'fas fa-check',
                        class: 'bg-success m-1',
                        autohide: true,
                        delay: 5000,
                        title: 'Informasi',
                        body: data
                    })
                },
                error:function (a) {
                    $(document).Toasts('create', {
                        icon: 'fas fa-exclamation-triangle',
			            class: 'bg-danger m-1',
                        autohide: true,
                        delay: 5000,
                        title: 'Informasi',
                        body: '"Tidak dapat terhubung dengan server. Mohon coba lagi."'
                    })
                }
            });
        });
        $('#hapusdata').on('submit', function (e) {
            e.preventDefault();
            var uri=$('#hapusdata').attr('action');
            $.ajax({
                type: 'POST',
                url: uri,
                data: $('#hapusdata').serialize(),
                success: function (data) {
                    document.getElementById("hapusdata").reset();
                    $('#modal-hapus').modal('hide');
                    table.ajax.url("{{route('notapenerimaandata')}}").load();
                    $(document).Toasts('create', {
                        icon: 'fas fa-check',
                        class: 'bg-success m-1',
                        autohide: true,
                        delay: 5000,
                        title: 'Informasi',
                        body: data
                    })
                },
                error:function (a) {
                    $(document).Toasts('create', {
                        icon: 'fas fa-exclamation-triangle',
			            class: 'bg-danger m-1',
                        autohide: true,
                        delay: 5000,
                        title: 'Informasi',
                        body: '"Tidak dapat terhubung dengan server. Mohon coba lagi."'
                    })
                }
            });
        });
    });

    function setvalue(){
        var bruto = parseFloat(document.getElementById("kodetimbangan").options[document.getElementById("kodetimbangan").selectedIndex].getAttribute('bruto')).toFixed(2); 
        var colly = parseInt(document.getElementById("kodetimbangan").options[document.getElementById("kodetimbangan").selectedIndex].getAttribute('colly')); 
        var pedagang = document.getElementById("kodetimbangan").options[document.getElementById("kodetimbangan").selectedIndex].getAttribute('pedagang'); 
        var nopol = document.getElementById("kodetimbangan").options[document.getElementById("kodetimbangan").selectedIndex].getAttribute('nopol'); 
        var item = document.getElementById("kodetimbangan").options[document.getElementById("kodetimbangan").selectedIndex].getAttribute('item'); 
        document.getElementById('colly').value=colly;
        document.getElementById('pedagang').value=pedagang;
        document.getElementById('nopol').value=nopol;
        document.getElementById('item').value=item;
        var collyraw = 0;
        var beratraw = 0;
        var jml = 0;
        var package = document.getElementsByClassName('kodepackage');
        var jmlpackage = document.getElementsByClassName('jumlahpackage');
        for(var a=0;a<package.length;a++){
            var b = parseFloat(package[a].options[package[a].selectedIndex].getAttribute('berat'));
            beratraw=beratraw+b;
            if(jmlpackage[a].value!=null){
                jml = jml + parseFloat(jmlpackage[a].value);
            }
        }
        // if(jml!==colly){
        //     document.getElementById("simpantbn").style.display='none';
        //     alert("Jumlah Package harus sama dengan jumlah colly. Mohon untuk tidak mengisi sejumlah colly.");
        // } else {
            var tara = beratraw * jml;
            var netto = bruto-tara;
            document.getElementById('bruto').value=parseFloat(bruto);
            document.getElementById('tara').value=tara;
            document.getElementById('netto').value=netto.toFixed(2);
            document.getElementById("simpantbn").style.display='block';
        // }
    }

    function addrow(){
        $('#bodytr').append('<tr><td><select name="kodepackage[]" class="form-control kodepackage" onchange="setvalue()" required><option>Pilih Kode Package</option>@foreach(\App\Models\Package::get() as $data)<option value="{{$data->id}}" berat="{{$data->beratpackage}}">{{$data->namapackage}}</option>@endforeach</select></td><td><input class="form-control jumlahpackage" name="jumlahpackage[]" onchange="setvalue()" value=1 required></td><td><button type="button" class="btn btn-danger" onclick="deletetr(this)"><i class="fa fa-times"></i></button></td></tr>');
    }

    function setvalueedit(){
        var bruto = document.getElementById('brutoedit').value;
        var colly = document.getElementById('collyedit').value;
        var collyraw = 0;
        var beratraw = 0;
        var jml = 0;
        var package = document.getElementsByClassName('kodepackageedit');
        var jmlpackage = document.getElementsByClassName('jumlahpackageedit');
        for(var a=0;a<package.length;a++){
            var b = parseFloat(package[a].options[package[a].selectedIndex].getAttribute('berat'));
            beratraw=beratraw+b;
            if(jmlpackage[a].value!=null){
                jml = jml + parseFloat(jmlpackage[a].value);
            }
        }
        // if(jml!==parseInt(colly)){
        //     document.getElementById("simpantbnedit").style.display='none';
        //     alert("Jumlah Package harus sama dengan jumlah colly. Mohon untuk tidak mengisi sejumlah colly.");
        // } else {
            var tara = beratraw * jml;
            var netto = bruto-tara;
            document.getElementById('brutoedit').value=parseFloat(bruto);
            document.getElementById('taraedit').value=tara;
            document.getElementById('nettoedit').value=netto.toFixed(2);
            document.getElementById("simpantbnedit").style.display='block';
        // }
    }

    function addrowedit(){
        $('#bodytredit').append('<tr><td><select name="kodepackage[]" class="form-control kodepackageedit" onchange="setvalueedit()" required><option>Pilih Kode Package</option>@foreach(\App\Models\Package::get() as $data)<option value="{{$data->id}}" berat="{{$data->beratpackage}}">{{$data->namapackage}}</option>@endforeach</select></td><td><input class="form-control jumlahpackageedit" name="jumlahpackage[]" onchange="setvalueedit()" value=1 required></td><td><button type="button" class="btn btn-danger" onclick="deletetr(this)"><i class="fa fa-times"></i></button></td></tr>');
    }

    function deletetr(elem){
        console.log($(elem).closest('tr'));
        $(elem).closest('tr').remove();
    }
    
    function formatDate(date) {
        var d = new Date(date),
            month = '' + (d.getMonth() + 1),
            day = '' + d.getDate(),
            year = d.getFullYear();

        if (month.length < 2) 
            month = '0' + month;
        if (day.length < 2) 
            day = '0' + day;

        return [year, month, day].join('-');
    }
    function cetak(id,tgl) {
        var tanggal = formatDate(tgl.substring(1, 10));
        $('#idcetak').val(id);
        $('#tglcetak').val(tanggal);
        $("#modal-cetak").modal();
        console.log(tanggal,tgl)
    }

    function cetakya()
    {
        var tgl = document.getElementById('tglcetak').value;
        var id= document.getElementById('idcetak').value;
        var url = "{{route('cetaknotapenerimaan')}}?id="+id+"&tgl="+tgl;
        window.open(url, '_blank').focus();
        $('#modal-cetak').modal('hide');
    }
</script>
@stop