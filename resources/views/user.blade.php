@extends('adminlte::page')

@section('title', 'User')

@section('content_header')
<h1>User</h1>
@stop

@section('content')
<div class="row mb-3">
    <div class="col-md-12">
        @if ($message = Session::get('success'))
        <div class="alert alert-success alert-block">
            <button type="button" class="close" data-dismiss="alert">×</button>
            <strong>{{ $message }}</strong>
        </div>
        @endif
        @if ($message = Session::get('danger'))
        <div class="alert alert-danger alert-block">
            <button type="button" class="close" data-dismiss="alert">×</button>
            <strong>{{ $message }}</strong>
        </div>
        @endif
    </div>
</div>

<div class="row">
    <div class="col-12">
        <div class="card">
            <!-- /.card-header -->
            <div class="card-body">
                <table id="example1" class="table table-bordered table-striped dataTable" width="100%">
                    <thead>
                        <tr role="row">
                            <th>#</th>
                            <th>Nama Lengkap</th>
                            <th>Email</th>
                            <th>User Level</th>
                            <th>Kode Gudang</th>
                            <th>Dibuat Tanggal</th>
                            <th>Pilihan</th>
                        </tr>
                    </thead>
                </table>
            </div>
        </div>
        <!-- /.card-body -->
    </div>
</div>
</div>

<div class="modal fade" id="modal-xl">
    <div class="modal-dialog modal-xl">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title">Tambahkan user baru</h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <form method="POST" action="{{ route('userbaru') }}" id="savenewdata">
                <div class="modal-body">
                    <div class="row">
                        <div class="col-xs-12 col-md-4 mb-2">
                            <div class="form-group">
                                <label>Nama lengkap</label>
                                @csrf
                                <input type="text" class="form-control" name="fullname" required>
                            </div>
                        </div>
                        <div class="col-xs-12 col-md-4 mb-2">
                            <div class="form-group">
                                <label>Email</label>
                                <input type="text" class="form-control" name="email" required>
                            </div>
                        </div>
                        <div class="col-xs-12 col-md-4 mb-2">
                            <div class="form-group">
                                <label>Password</label>
                                <input type="password" class="form-control" name="password" required>
                            </div>
                        </div>
                        <div class="col-xs-12 col-md-4 mb-2">
                            <div class="form-group">
                                <label>User Level</label>
                                <select name="role" class="form-control" required>
                                    <option>Pilih Level</option>
                                    <option value="Administrator">Administrator</option>
                                    <option value="Penimbang">Penimbang</option>
                                    <option value="Staff">Staff</option>
                                </select>
                            </div>
                        </div>
                        <div class="col-xs-12 col-md-4 mb-2">
                            <div class="form-group">
                                <label>Kode Gudang</label>
                                <select name="kodegudang" class="form-control" required>
                                    <option>Pilih Gudang</option>
                                    @foreach(\App\Models\Warehouse::get() as $data)
                                    <option value="{{$data->kodegudang}}">{{$data->namagudang}}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer justify-content-between">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    <button type="submit" class="btn btn-primary">Save</button>
                </div>
            </form>
        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>
<div class="modal fade" id="modal-edit">
    <div class="modal-dialog modal-xl">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title">Ubah data user</h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <form method="POST" action="{{ route('edituser') }}" id="editdata">
                <div class="modal-body">
                    <div class="row">
                        <div class="col-xs-12 col-md-4 mb-2">
                            <div class="form-group">
                                <label>Nama lengkap</label>
                                @csrf
                                <input type="text" class="form-control" name="fullname" id="fullname" required>
                                <input type="hidden" class="form-control" name="id" id="id" required>
                            </div>
                        </div>
                        <div class="col-xs-12 col-md-4 mb-2">
                            <div class="form-group">
                                <label>Email</label>
                                <input type="text" class="form-control" name="email" id="email" required>
                            </div>
                        </div>
                        <div class="col-xs-12 col-md-4 mb-2">
                            <div class="form-group">
                                <label>User Level</label>
                                <select name="role" class="form-control" required id="role">
                                    <option>Pilih Level</option>
                                    <option value="Administrator">Administrator</option>
                                    <option value="Penimbang">Penimbang</option>
                                    <option value="Staff">Staff</option>
                                </select>
                            </div>
                        </div>
                        <div class="col-xs-12 col-md-4 mb-2">
                            <div class="form-group">
                                <label>Kode Gudang</label>
                                <select name="kodegudang" class="form-control" required id="kodegudang">
                                    <option>Pilih Gudang</option>
                                    @foreach(\App\Models\Warehouse::get() as $data)
                                    <option value="{{$data->kodegudang}}">{{$data->namagudang}}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer justify-content-between">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    <button type="submit" class="btn btn-primary">Save</button>
                </div>
            </form>
        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>
<div class="modal fade" id="modal-editpassword">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title">Ubah data password user</h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <form method="POST" action="{{ route('edituserpassword') }}" id="resetpassword">
                <div class="modal-body">
                    <div class="row">
                        <div class="col-md-12 mb-2">
                            <label>Reset Password</label>
                            @csrf
                            <input type="password" class="form-control" name="current_password" required>
                            <input type="hidden" class="form-control" name="id" id="idpassword">
                        </div>
                    </div>
                </div>
                <div class="modal-footer justify-content-between">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    <button type="submit" class="btn btn-primary">Save</button>
                </div>
            </form>
        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>
<div class="modal fade" id="modal-hapus">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title">Hapus Data</h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <form method="POST" action="{{ route('hapususer') }}" id="hapusdata">
                <div class="modal-body">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="form-group">
                                Apakah anda yakin ingin menghapus data?
                                @csrf
                                <input type="hidden" class="form-control" name="id" id="hapusid">
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Tidak</button>
                    <button type="submit" class="btn btn-danger">Ya</button>
                </div>
            </form>
        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>
<!-- /.modal -->
@stop

@section('css')
<link rel="stylesheet" href="vendor/datatables/css/dataTables.bootstrap4.css">
<link rel="stylesheet" href="vendor/datatables-plugins/buttons/css/buttons.bootstrap4.min.css">
<link rel="stylesheet" href="vendor/daterangepicker/daterangepicker.css">
@stop

@section('js')
<script src="vendor/datatables/js/jquery.dataTables.min.js"></script>
<script src="vendor/datatables/js/dataTables.bootstrap4.min.js"></script>
<script src="vendor/datatables-plugins/buttons/js/buttons.bootstrap4.min.js"></script>
<script src="vendor/datatables-plugins/buttons/js/dataTables.buttons.min.js"></script>
<script src="vendor/datatables-plugins/buttons/js/buttons.colVis.min.js"></script>
<script src="vendor/datatables-plugins/buttons/js/buttons.flash.min.js"></script>
<script src="vendor/datatables-plugins/buttons/js/buttons.html5.min.js"></script>
<script src="vendor/datatables-plugins/buttons/js/buttons.print.min.js"></script>
<script src="vendor/daterangepicker/moment.min.js"></script>
<script src="vendor/daterangepicker/daterangepicker.js"></script>
<script>
var table = $("#example1").DataTable({
        responsive: true,
        dom: 'Bfrtip',
        buttons: [{
                text: 'User Baru',
                action: function(e, dt, node, config) {
                    $("#modal-xl").modal();
                }
            },
            'copy', 'csv', 'excel', 'pdf', 'print'
        ],
        "oLanguage": {
                "sSearch": "Pencarian",
                "sLengthMenu": "Tampilkan _MENU_ data per halaman",
                "sLoadingRecords": "Mohon tunggu - sedang mengambil data dari server...",
                "sEmptyTable": "Belum ada data...",
                "sInfoEmpty": "Menampilkan 0 - 0 dari 0 data",
                "sInfo": "Menampilkan _START_ - _END_ dari _TOTAL_ data",
                "sInfoFiltered":   "(Dicari dari _MAX_ total data)",
                "sZeroRecords":    "Data yang dicari tidak ada yang sesuai",
                "oPaginate": {                       
                        "sNext": '<i class="fa fa-angle-right" ></i>',
                        "sPrevious": '<i class="fa fa-angle-left"" ></i>'
                },
            },
            ajax:"{{route('datauser')}}",
            columns: [
                { data: "id" },
                { data: "fullname" },
                { data: "email" },
                { data: "userlevel" },
                { data: "kodegudang" },
                { data: "date" },
                { data: "id" }
            ],
            "order": [[ 0, "desc" ]],
            scrollX: true,
            scrollCollapse: true,
            columnDefs: [{
                    targets: -1,
                    title: 'Opsi',
                    orderable: false,
                    width: '140px',
                    render: function(data, type, full, meta) {
                         return '\
                            <a href="javascript:;" class="btn btn-sm btn-clean btn-icon mr-2 editbtn" title="Edit" onclick="edit('+data+')">\
                                <span class="svg-icon svg-icon-md">\
                                    <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1">\
                                        <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">\
                                            <rect x="0" y="0" width="24" height="24"/>\
                                            <path d="M8,17.9148182 L8,5.96685884 C8,5.56391781 8.16211443,5.17792052 8.44982609,4.89581508 L10.965708,2.42895648 C11.5426798,1.86322723 12.4640974,1.85620921 13.0496196,2.41308426 L15.5337377,4.77566479 C15.8314604,5.0588212 16,5.45170806 16,5.86258077 L16,17.9148182 C16,18.7432453 15.3284271,19.4148182 14.5,19.4148182 L9.5,19.4148182 C8.67157288,19.4148182 8,18.7432453 8,17.9148182 Z" fill="#000000" fill-rule="nonzero"\ transform="translate(12.000000, 10.707409) rotate(-135.000000) translate(-12.000000, -10.707409) "/>\
                                            <rect fill="#000000" opacity="0.3" x="5" y="20" width="15" height="2" rx="1"/>\
                                        </g>\
                                    </svg>\
                                </span>\
                            </a>\
                            <a href="javascript:;" class="btn btn-sm btn-clean btn-icon mr-2 editbtn" title="Ubah Password" onclick="pass('+data+')">\
                                <i class="fa fa-key"></i>\
                            </a>\
                            <a href="javascript:;" class="btn btn-sm btn-clean btn-icon hapus" title="Hapus" onclick="hapus('+data+')" data-toggle="modal" data-target="#"modal-hapus">\
                                <span class="svg-icon svg-icon-md">\
                                    <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1">\
                                        <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">\
                                            <rect x="0" y="0" width="24" height="24"/>\
                                            <path d="M6,8 L6,20.5 C6,21.3284271 6.67157288,22 7.5,22 L16.5,22 C17.3284271,22 18,21.3284271 18,20.5 L18,8 L6,8 Z" fill="#000000" fill-rule="nonzero"/>\
                                            <path d="M14,4.5 L14,4 C14,3.44771525 13.5522847,3 13,3 L11,3 C10.4477153,3 10,3.44771525 10,4 L10,4.5 L5.5,4.5 C5.22385763,4.5 5,4.72385763 5,5 L5,5.5 C5,5.77614237 5.22385763,6 5.5,6 L18.5,6 C18.7761424,6 19,5.77614237 19,5.5 L19,5 C19,4.72385763 18.7761424,4.5 18.5,4.5 L14,4.5 Z" fill="#000000" opacity="0.3"/>\
                                        </g>\
                                    </svg>\
                                </span>\
                            </a>\
                        ';
                    },
                },
                {
                    targets: 0,
                    render: function(data, type, full, meta) {
                        return meta.row + 1;
                    },
                }
            ],
    });

    function edit(id){
        $.ajax({
            url: "{{route('userdata')}}",
            cache: false,
            dataType: 'json',
            data: {id:id},
            success: function(data){
                $("#modal-edit").modal();
                document.getElementById('id').value = id;
                document.getElementById('fullname').value = data['fullname'];
                document.getElementById('email').value = data['email'];
                document.getElementById('role').value = data['userlevel'];
                document.getElementById('kodegudang').value = data['kodegudang'];
            }
        });
    }

    function hapus(hapus) {
        $('#hapusid').val(hapus);
        $("#modal-hapus").modal();
    }

    function pass(hapus) {
        $('#idpassword').val(hapus);
        $("#modal-editpassword").modal();
    }

    $(function() {  
        $('#savenewdata').on('submit', function (e) {
            e.preventDefault();
            var uri=$('#savenewdata').attr('action');
            $.ajax({
                type: 'POST',
                url: uri,
                data: $('#savenewdata').serialize(),
                success: function (data) {
                    document.getElementById("savenewdata").reset();
                    $('#modal-xl').modal('hide');
                    table.ajax.url("{{route('datauser')}}").load();
                    $(document).Toasts('create', {
                        icon: 'fas fa-check',
                        class: 'bg-success m-1',
                        autohide: true,
                        delay: 5000,
                        title: 'Informasi',
                        body: data
                    })
                },
                error:function (a) {
                    $(document).Toasts('create', {
                        icon: 'fas fa-exclamation-triangle',
			            class: 'bg-danger m-1',
                        autohide: true,
                        delay: 5000,
                        title: 'Informasi',
                        body: '"Tidak dapat terhubung dengan server. Mohon coba lagi."'
                    })
                }
            });
        });
        $('#editdata').on('submit', function (e) {
            e.preventDefault();
            var uri=$('#editdata').attr('action');
            $.ajax({
                type: 'POST',
                url: uri,
                data: $('#editdata').serialize(),
                success: function (data) {
                    document.getElementById("editdata").reset();
                    $('#modal-edit').modal('hide');
                    table.ajax.url("{{route('datauser')}}").load();
                    $(document).Toasts('create', {
                        icon: 'fas fa-check',
                        class: 'bg-success m-1',
                        autohide: true,
                        delay: 5000,
                        title: 'Informasi',
                        body: data
                    })
                },
                error:function (a) {
                    $(document).Toasts('create', {
                        icon: 'fas fa-exclamation-triangle',
			            class: 'bg-danger m-1',
                        autohide: true,
                        delay: 5000,
                        title: 'Informasi',
                        body: '"Tidak dapat terhubung dengan server. Mohon coba lagi."'
                    })
                }
            });
        });
        $('#hapusdata').on('submit', function (e) {
            e.preventDefault();
            var uri=$('#hapusdata').attr('action');
            $.ajax({
                type: 'POST',
                url: uri,
                data: $('#hapusdata').serialize(),
                success: function (data) {
                    document.getElementById("hapusdata").reset();
                    $('#modal-hapus').modal('hide');
                    table.ajax.url("{{route('datauser')}}").load();
                    $(document).Toasts('create', {
                        icon: 'fas fa-check',
                        class: 'bg-success m-1',
                        autohide: true,
                        delay: 5000,
                        title: 'Informasi',
                        body: data
                    })
                },
                error:function (a) {
                    $(document).Toasts('create', {
                        icon: 'fas fa-exclamation-triangle',
			            class: 'bg-danger m-1',
                        autohide: true,
                        delay: 5000,
                        title: 'Informasi',
                        body: '"Tidak dapat terhubung dengan server. Mohon coba lagi."'
                    })
                }
            });
        });
        $('#resetpassword').on('submit', function (e) {
            e.preventDefault();
            var uri=$('#resetpassword').attr('action');
            $.ajax({
                type: 'POST',
                url: uri,
                data: $('#resetpassword').serialize(),
                success: function (data) {
                    document.getElementById("resetpassword").reset();
                    $('#modal-editpassword').modal('hide');
                    table.ajax.url("{{route('datauser')}}").load();
                    $(document).Toasts('create', {
                        icon: 'fas fa-check',
                        class: 'bg-success m-1',
                        autohide: true,
                        delay: 5000,
                        title: 'Informasi',
                        body: data
                    })
                },
                error:function (a) {
                    $(document).Toasts('create', {
                        icon: 'fas fa-exclamation-triangle',
			            class: 'bg-danger m-1',
                        autohide: true,
                        delay: 5000,
                        title: 'Informasi',
                        body: '"Tidak dapat terhubung dengan server. Mohon coba lagi."'
                    })
                }
            });
        });
    });
</script>
@stop