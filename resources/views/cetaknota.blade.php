<!DOCTYPE html>
<html lang="id">
<head>
  <title>Cetak Nota</title>
</head>
<body align="center">

Weight Bill<br>
<?php echo isset($_GET['tgl'])?date_format(date_create($_GET['tgl']),'d/m/Y'):date('d/m/Y'); ?><br>
<?php echo $header->createdfrom; ?><br>
<?php echo $header->location; ?><br>
<?php echo $data->tipe; ?><br>
<?php echo $data->pedagang; ?><br>
<?php echo $data->nopol; ?><br>
<?php echo $data->namaitem; ?><br>
-------------------<br><br>
<table align="center">
<?php
$i=1;
foreach ($timbangan as $t) {
    ?>
    <tr>
        <td align="left" width="60px">#<?= sprintf("%03d", $i) ?></td>
        <td align="right"><?= number_format($t->quantitydetails,2).'&nbsp;KG' ?></td>
    </tr>
<?php 
    $i++;
    } 
?>
</table>
<br>-------------------<br>
<table align="center">
    <tr>
        <td align="left">Accum</td>
        <td align="left">= <?php echo number_format($jumlah,2); ?>&nbsp;KG</td>
    </tr>
    <tr>
        <td align="left">Tara</td>
        <td align="left">= <?php echo number_format($header->tara,2); ?>&nbsp;KG</td>
    </tr>
    <tr>
        <td align="left">Netto</td>
        <td align="left">= <?php echo number_format($header->netto,2); ?>&nbsp;KG</td>
    </tr>
    
    <tr>
        <td align="left">Colly</td>
        <td align="left">= <?php echo $i-1; ?></td>
    </tr>
</table>
</body>
</html>