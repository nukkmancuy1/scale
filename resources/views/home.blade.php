@extends('adminlte::page')

@section('title', 'Dashboard')

@section('content_header')
<h1>Dashboard</h1>
@stop

@section('content')
<h5 class="mb-2">Master Data</h5>
<div class="row">
    <div class="col-md-3 col-sm-6 col-12">
        <div class="info-box">
            <span class="info-box-icon bg-info"><i class="fa fa-users"></i></span>

            <div class="info-box-content">
                <span class="info-box-text">Total User</span>
                <span class="info-box-number">
                    <?php 
                        $data = \App\Models\User::count();
                        echo number_format($data,0);
                    ?>
                </span>
            </div>
            <!-- /.info-box-content -->
        </div>
        <!-- /.info-box -->
    </div>
    <!-- /.col -->
    <div class="col-md-3 col-sm-6 col-12">
        <div class="info-box">
            <span class="info-box-icon bg-info"><i class="fa fa-home"></i></span>

            <div class="info-box-content">
                <span class="info-box-text">Total Warehouse</span>
                <span class="info-box-number">
                <?php 
                        $data = \App\Models\Warehouse::count();
                        echo number_format($data,0);
                    ?>
                </span>
            </div>
            <!-- /.info-box-content -->
        </div>
        <!-- /.info-box -->
    </div>
    <!-- /.col -->
    <div class="col-md-3 col-sm-6 col-12">
        <div class="info-box">
            <span class="info-box-icon bg-info"><i class="fa fa-box"></i></span>

            <div class="info-box-content">
                <span class="info-box-text">Total Package</span>
                <span class="info-box-number">
                    <?php 
                        $data = \App\Models\Package::count();
                        echo number_format($data,0);
                    ?>
                </span>
            </div>
            <!-- /.info-box-content -->
        </div>
        <!-- /.info-box -->
    </div>
    <!-- /.col -->
    <div class="col-md-3 col-sm-6 col-12">
        <div class="info-box">
            <span class="info-box-icon bg-info"><i class="fa fa-box-open"></i></span>

            <div class="info-box-content">
                <span class="info-box-text">Total Item</span>
                <span class="info-box-number">
                    <?php 
                        $data = \App\Models\Item::count();
                        echo number_format($data,0);
                    ?>
                </span>
            </div>
            <!-- /.info-box-content -->
        </div>
        <!-- /.info-box -->
    </div>
    <!-- /.col -->
</div>
<h5 class="mb-2">Timbangan</h5>
<div class="row">
    <div class="col-md-3 col-sm-6 col-12">
        <div class="info-box">
            <span class="info-box-icon bg-info"><i class="fa fa-balance-scale"></i></span>

            <div class="info-box-content">
                <span class="info-box-text">Total Timbangan</span>
                <span class="info-box-number">
                    <?php 
                        $data = \App\Models\Datatimbangan::count();
                        echo number_format($data,0);
                    ?>
                </span>
            </div>
            <!-- /.info-box-content -->
        </div>
        <!-- /.info-box -->
    </div>
    <!-- /.col -->
    <div class="col-md-3 col-sm-6 col-12">
        <div class="info-box">
            <span class="info-box-icon bg-info"><i class="fa fa-balance-scale"></i></span>

            <div class="info-box-content">
                <span class="info-box-text">Total Timbangan Bulan Ini</span>
                <span class="info-box-number">
                    <?php 
                        $m = date('m');
                        $y = date('Y');
                        $data = \App\Models\Datatimbangan::whereMonth('created_at',$m)->whereYear('created_at',$y)->get()->count();
                        echo number_format($data,0);
                    ?>
                </span>
            </div>
            <!-- /.info-box-content -->
        </div>
        <!-- /.info-box -->
    </div>
    <!-- /.col -->
    <div class="col-md-3 col-sm-6 col-12">
        <div class="info-box">
            <span class="info-box-icon bg-info"><i class="fa fa-balance-scale"></i></span>

            <div class="info-box-content">
                <span class="info-box-text">Timbangan Penerimaan</span>
                <span class="info-box-number">
                    <?php 
                        $m = date('m');
                        $y = date('Y');
                        $data = \App\Models\Datatimbangan::whereMonth('created_at',$m)->whereYear('created_at',$y)->where('tipe','Penerimaan')->get()->count();
                        echo number_format($data,0);
                    ?>
                </span>
            </div>
            <!-- /.info-box-content -->
        </div>
        <!-- /.info-box -->
    </div>
    <!-- /.col -->
    <!-- /.col -->
    <div class="col-md-3 col-sm-6 col-12">
        <div class="info-box">
            <span class="info-box-icon bg-info"><i class="fa fa-balance-scale"></i></span>

            <div class="info-box-content">
                <span class="info-box-text">Timbangan Pengiriman</span>
                <span class="info-box-number">
                    <?php 
                        $m = date('m');
                        $y = date('Y');
                        $data = \App\Models\Datatimbangan::whereMonth('created_at',$m)->whereYear('created_at',$y)->where('tipe','Pengiriman')->get()->count();
                        echo number_format($data,0);
                    ?>
                </span>
            </div>
            <!-- /.info-box-content -->
        </div>
        <!-- /.info-box -->
    </div>
</div>
<h5 class="mb-2">Nota Penerimaan</h5>
<div class="row">
    <div class="col-md-3 col-sm-6 col-12">
        <div class="info-box">
            <span class="info-box-icon bg-info"><i class="fa fa-file-alt"></i></span>

            <div class="info-box-content">
                <span class="info-box-text">Total Nota</span>
                <span class="info-box-number">
                    <?php 
                        $data = \App\Models\Notapenerimaan::count();
                        echo number_format($data,0);
                    ?>
                </span>
            </div>
            <!-- /.info-box-content -->
        </div>
        <!-- /.info-box -->
    </div>
    <!-- /.col -->
    <div class="col-md-3 col-sm-6 col-12">
        <div class="info-box">
            <span class="info-box-icon bg-info"><i class="fa fa-file-alt"></i></span>

            <div class="info-box-content">
                <span class="info-box-text">Total Nota Tahun Ini</span>
                <span class="info-box-number">
                    <?php 
                        $data = \App\Models\Notapenerimaan::whereYear('created_at',date('Y'))->count();
                        echo number_format($data,0);
                    ?>
                </span>
            </div>
            <!-- /.info-box-content -->
        </div>
        <!-- /.info-box -->
    </div>
    <!-- /.col -->
    <div class="col-md-3 col-sm-6 col-12">
        <div class="info-box">
            <span class="info-box-icon bg-info"><i class="fa fa-file-alt"></i></span>

            <div class="info-box-content">
                <span class="info-box-text">Total Nota Bulan Ini</span>
                <span class="info-box-number">
                <?php 
                        $data = \App\Models\Notapenerimaan::whereMonth('created_at',date('m'))->whereYear('created_at',date('Y'))->count();
                        echo number_format($data,0);
                    ?>
                </span>
            </div>
            <!-- /.info-box-content -->
        </div>
        <!-- /.info-box -->
    </div>
    <!-- /.col -->
    <div class="col-md-3 col-sm-6 col-12">
        <div class="info-box">
            <span class="info-box-icon bg-info"><i class="fa fa-balance-scale"></i></span>

            <div class="info-box-content">
                <span class="info-box-text">Total Nota Hari ini</span>
                <span class="info-box-number">
                <?php 
                        $data = \App\Models\Notapenerimaan::whereDay('created_at',date('d'))->whereMonth('created_at',date('m'))->whereYear('created_at',date('Y'))->count();
                        echo number_format($data,0);
                    ?>
                </span>
            </div>
            <!-- /.info-box-content -->
        </div>
        <!-- /.info-box -->
    </div>
    <!-- /.col -->
</div>
<h5 class="mb-2">Nota Pengiriman</h5>
<div class="row">
    <div class="col-md-3 col-sm-6 col-12">
        <div class="info-box">
            <span class="info-box-icon bg-info"><i class="fa fa-file-alt"></i></span>

            <div class="info-box-content">
                <span class="info-box-text">Total Nota</span>
                <span class="info-box-number">
                    <?php 
                        $data = \App\Models\Notapengiriman::count();
                        echo number_format($data,0);
                    ?>
                </span>
            </div>
            <!-- /.info-box-content -->
        </div>
        <!-- /.info-box -->
    </div>
    <!-- /.col -->
    <div class="col-md-3 col-sm-6 col-12">
        <div class="info-box">
            <span class="info-box-icon bg-info"><i class="fa fa-file-alt"></i></span>

            <div class="info-box-content">
                <span class="info-box-text">Total Nota Tahun Ini</span>
                <span class="info-box-number">
                    <?php 
                        $data = \App\Models\Notapengiriman::whereYear('created_at',date('Y'))->count();
                        echo number_format($data,0);
                    ?>
                </span>
            </div>
            <!-- /.info-box-content -->
        </div>
        <!-- /.info-box -->
    </div>
    <!-- /.col -->
    <div class="col-md-3 col-sm-6 col-12">
        <div class="info-box">
            <span class="info-box-icon bg-info"><i class="fa fa-file-alt"></i></span>

            <div class="info-box-content">
                <span class="info-box-text">Total Nota Bulan Ini</span>
                <span class="info-box-number">
                <?php 
                        $data = \App\Models\Notapengiriman::whereMonth('created_at',date('m'))->whereYear('created_at',date('Y'))->count();
                        echo number_format($data,0);
                    ?>
                </span>
            </div>
            <!-- /.info-box-content -->
        </div>
        <!-- /.info-box -->
    </div>
    <!-- /.col -->
    <div class="col-md-3 col-sm-6 col-12">
        <div class="info-box">
            <span class="info-box-icon bg-info"><i class="fa fa-balance-scale"></i></span>

            <div class="info-box-content">
                <span class="info-box-text">Total Nota Hari ini</span>
                <span class="info-box-number">
                <?php 
                        $data = \App\Models\Notapengiriman::whereDay('created_at',date('d'))->whereMonth('created_at',date('m'))->whereYear('created_at',date('Y'))->count();
                        echo number_format($data,0);
                    ?>
                </span>
            </div>
            <!-- /.info-box-content -->
        </div>
        <!-- /.info-box -->
    </div>
    <!-- /.col -->
</div>
@stop

@section('css')
<link rel="stylesheet" href="/css/admin_custom.css">
@stop

@section('js')
<script>
console.log('Hi!');
</script>
@stop