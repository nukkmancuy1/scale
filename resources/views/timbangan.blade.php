@extends('adminlte::page')

@section('title', 'Timbangan')

@section('content_header')
<h1>Timbangan</h1>
@stop

@section('content')
<div class="row">
    <div class="col-12">
        <div class="card">
            <div class="card-header">
                <div class="row">
                    <div class="col-xs-12 col-md-8">&nbsp;</div>
                    <div class="col-xs-12 col-md-1 pt-2">
                        Periode : 
                    </div>
                    <div class="col-xs-12 col-md-3">
                        <input type="text" class="form-control" id="daterangepick">
                    </div>
                </div>
            </div>
            <div class="card-body">
                <table id="example1" class="table table-bordered table-striped dataTable" width="100%">
                    <thead>
                        <tr role="row">
                            <th width="5%">#</th>
                            <th>Kode</th>
                            <th>Pedagang</th>
                            <th>No. Truk</th>
                            <th>Tipe</th>
                            <th>Item</th>
                            <th>Colly</th>
                            <th>Bruto (Kg)</th>
                            <th>Dibuat Tanggal</th>
                            <th>Pilihan</th>
                        </tr>
                    </thead>
                </table>
            </div>
        </div>
        <!-- /.card-body -->
    </div>
</div>
</div>
<div class="modal fade" id="modal-hapus">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title">Hapus Data</h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <form method="POST" action="{{ route('hapustimbangan') }}" id="hapusdata">
                <div class="modal-body">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="form-group">
                                Apakah anda yakin ingin menghapus data?
                                @csrf
                                <input type="hidden" class="form-control" name="id" id="hapusid">
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Tidak</button>
                    <button type="submit" class="btn btn-danger">Ya</button>
                </div>
            </form>
        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>
<!-- /.modal -->
<div class="modal fade" id="modal-cetak">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title">Cetak Data</h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="col-md-12">
                        <div class="form-group">
                            <label>Tanggal Cetak</label>
                            <input type="date" class="form-control" name="tglcetak" id="tglcetak">
                            <input type="hidden" class="form-control" name="idcetak" id="idcetak">
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Tidak</button>
                <button type="button" class="btn btn-danger" onclick="cetakya()">Ya</button>
            </div>
        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>
<!-- /.modal -->
@stop

@section('css')
<link rel="stylesheet" href="vendor/datatables/css/dataTables.bootstrap4.css">
<link rel="stylesheet" href="vendor/datatables-plugins/buttons/css/buttons.bootstrap4.min.css">
<link rel="stylesheet" href="vendor/daterangepicker/daterangepicker.css">
@stop

@section('js')
<script src="vendor/datatables/js/jquery.dataTables.min.js"></script>
<script src="vendor/datatables/js/dataTables.bootstrap4.min.js"></script>
<script src="vendor/datatables-plugins/buttons/js/buttons.bootstrap4.min.js"></script>
<script src="vendor/datatables-plugins/buttons/js/dataTables.buttons.min.js"></script>
<script src="vendor/datatables-plugins/buttons/js/buttons.colVis.min.js"></script>
<script src="vendor/datatables-plugins/buttons/js/buttons.flash.min.js"></script>
<script src="vendor/datatables-plugins/buttons/js/buttons.html5.min.js"></script>
<script src="vendor/datatables-plugins/buttons/js/buttons.print.min.js"></script>
<script src="vendor/daterangepicker/moment.min.js"></script>
<script src="vendor/daterangepicker/daterangepicker.js"></script>
<script>
    var start = moment();
    var end = moment();
    $(function() {
        function cb(started, ended) {
            $('#daterangepick span').html(started.format('MMMM D, YYYY') + ' - ' + ended.format('MMMM D, YYYY'));
            start=started;
            end=ended;
            var awal=start.format('YYYY-MM-D');
            var akhir=end.format('YYYY-MM-D');
            table.ajax.url("{{route('timbangandata')}}?awal="+awal+"&akhir="+akhir).load();
        }

        $('#daterangepick').daterangepicker({
            startDate: start,
            endDate: end,
            ranges: {
            'Today': [moment(), moment()],
            'Yesterday': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
            'Last 7 Days': [moment().subtract(6, 'days'), moment()],
            'Last 30 Days': [moment().subtract(29, 'days'), moment()],
            'This Month': [moment().startOf('month'), moment().endOf('month')],
            'Last Month': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
            }
        },cb);
        cb(start, end);
    });
</script>
<script>
    var table = $("#example1").DataTable({
        responsive: true,
        dom: 'Bfrtip',
        buttons: [{
                text: 'Timbangan Baru',
                action: function(e, dt, node, config) {
                    location.href = "{{route('timbanganbaru')}}";
                }
            },
            'copy', 'csv', 'excel', 'pdf', 'print'
        ],
        "oLanguage": {
                "sSearch": "Pencarian",
                "sLengthMenu": "Tampilkan _MENU_ data per halaman",
                "sLoadingRecords": "Mohon tunggu - sedang mengambil data dari server...",
                "sEmptyTable": "Belum ada data...",
                "sInfoEmpty": "Menampilkan 0 - 0 dari 0 data",
                "sInfo": "Menampilkan _START_ - _END_ dari _TOTAL_ data",
                "sInfoFiltered":   "(Dicari dari _MAX_ total data)",
                "sZeroRecords":    "Data yang dicari tidak ada yang sesuai",
                "oPaginate": {                       
                        "sNext": '<i class="fa fa-angle-right" ></i>',
                        "sPrevious": '<i class="fa fa-angle-left"" ></i>'
                },
            },
            ajax:"{{route('timbangandata')}}",
            columns: [
                { data: "kodetimbangan" },
                { data: "kodetimbangan" },
                { data: "pedagang" },
                { data: "nopol" },
                { data: "tipe" },
                { data: "namaitem" },
                { data: "totaldata" },
                { data: "bruto" },
                { data: "date" },
                { data: "kodetimbangan" }
            ],
            scrollX: true,
            scrollCollapse: true,
            columnDefs: [{
                    targets: -1,
                    title: 'Opsi',
                    orderable: false,
                    width: '170px',
                    render: function(data, type, full, meta) {
                        var datas="'"+data+"'";
                        var tgl="'"+full.date+"'";
                        return '\
                            <a href="javascript:;" class="btn btn-sm btn-clean btn-icon mr-2 editbtn" title="Edit" onclick="edit('+datas+')">\
                                <span class="svg-icon svg-icon-md">\
                                    <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1">\
                                        <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">\
                                            <rect x="0" y="0" width="24" height="24"/>\
                                            <path d="M8,17.9148182 L8,5.96685884 C8,5.56391781 8.16211443,5.17792052 8.44982609,4.89581508 L10.965708,2.42895648 C11.5426798,1.86322723 12.4640974,1.85620921 13.0496196,2.41308426 L15.5337377,4.77566479 C15.8314604,5.0588212 16,5.45170806 16,5.86258077 L16,17.9148182 C16,18.7432453 15.3284271,19.4148182 14.5,19.4148182 L9.5,19.4148182 C8.67157288,19.4148182 8,18.7432453 8,17.9148182 Z" fill="#000000" fill-rule="nonzero"\ transform="translate(12.000000, 10.707409) rotate(-135.000000) translate(-12.000000, -10.707409) "/>\
                                            <rect fill="#000000" opacity="0.3" x="5" y="20" width="15" height="2" rx="1"/>\
                                        </g>\
                                    </svg>\
                                </span>\
                            </a>\
                            <a href="javascript:;" target="_blank" class="btn btn-sm btn-clean btn-icon mr-2" title="Cetak" onclick="cetak('+datas+','+tgl+')" data-toggle="modal" data-target="#"modal-hapus">\
                                <i class="fa fa-print"></i>\
                            </a>\
                            <a href="javascript:;" class="btn btn-sm btn-clean btn-icon hapus" title="Hapus" onclick="hapus('+datas+')" data-toggle="modal" data-target="#"modal-hapus">\
                                <span class="svg-icon svg-icon-md">\
                                    <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1">\
                                        <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">\
                                            <rect x="0" y="0" width="24" height="24"/>\
                                            <path d="M6,8 L6,20.5 C6,21.3284271 6.67157288,22 7.5,22 L16.5,22 C17.3284271,22 18,21.3284271 18,20.5 L18,8 L6,8 Z" fill="#000000" fill-rule="nonzero"/>\
                                            <path d="M14,4.5 L14,4 C14,3.44771525 13.5522847,3 13,3 L11,3 C10.4477153,3 10,3.44771525 10,4 L10,4.5 L5.5,4.5 C5.22385763,4.5 5,4.72385763 5,5 L5,5.5 C5,5.77614237 5.22385763,6 5.5,6 L18.5,6 C18.7761424,6 19,5.77614237 19,5.5 L19,5 C19,4.72385763 18.7761424,4.5 18.5,4.5 L14,4.5 Z" fill="#000000" opacity="0.3"/>\
                                        </g>\
                                    </svg>\
                                </span>\
                            </a>\
                        ';
                    },
                },
                {
                    targets: 0,
                    render: function(data, type, full, meta) {
                        return meta.row + 1;
                    },
                }
            ],
    });

    window.indexedDB = window.indexedDB || window.mozIndexedDB || 
    window.webkitIndexedDB || window.msIndexedDB;
    
    //prefixes of window.IDB objects
    window.IDBTransaction = window.IDBTransaction || 
    window.webkitIDBTransaction || window.msIDBTransaction;
    window.IDBKeyRange = window.IDBKeyRange || window.webkitIDBKeyRange || 
    window.msIDBKeyRange
    
    if (!window.indexedDB) {
    window.alert("Your browser doesn't support a stable version of IndexedDB.")
    }
    
    // const employeeData = [
    // { berat: 20, datetime: '2021-11-13 10:00'},
    // { berat: 23, datetime: '2021-11-13 11:00'},
    // ];
    var db;
    var request = window.indexedDB.open("edittimbangan", 1);
    
    function edit(id){
        indexedDB.deleteDatabase('edittimbangan');
        location.href = "{{route('timbanganedit')}}/?kode="+id;
    }

    function hapus(hapus) {
        $('#hapusid').val(hapus);
        $("#modal-hapus").modal();
    }

    function formatDate(date) {
    var d = new Date(date),
        month = '' + (d.getMonth() + 1),
        day = '' + d.getDate(),
        year = d.getFullYear();

    if (month.length < 2) 
        month = '0' + month;
    if (day.length < 2) 
        day = '0' + day;

    return [year, month, day].join('-');
}
    function cetak(id,tgl) {
        var tanggal = formatDate(tgl.substring(1, 10));
        $('#idcetak').val(id);
        $('#tglcetak').val(tanggal);
        $("#modal-cetak").modal();
        console.log(tanggal,tgl)
    }

    function cetakya()
    {
        var tgl = document.getElementById('tglcetak').value;
        var kode= document.getElementById('idcetak').value;
        var url = "{{route('cetaksemua')}}?kode="+kode+"&tgl="+tgl;
        window.open(url, '_blank').focus();
        $('#modal-cetak').modal('hide');
    }
    
    $(function() {  
        $('#hapusdata').on('submit', function (e) {        
            e.preventDefault();
            var awal=start.format('YYYY-MM-D');
            var akhir=end.format('YYYY-MM-D');
            var uri2 = "{{route('timbangandata')}}?awal="+awal+"&akhir="+akhir;
            var uri=$('#hapusdata').attr('action');
            $.ajax({
                type: 'POST',
                url: uri,
                data: $('#hapusdata').serialize(),
                success: function (data) {
                    document.getElementById("hapusdata").reset();
                    $('#modal-hapus').modal('hide');
                    table.ajax.url(uri2).load();
                    $(document).Toasts('create', {
                        icon: 'fas fa-check',
                        class: 'bg-success m-1',
                        autohide: true,
                        delay: 5000,
                        title: 'Informasi',
                        body: data
                    })
                },
                error:function (a) {
                    $(document).Toasts('create', {
                        icon: 'fas fa-exclamation-triangle',
			            class: 'bg-danger m-1',
                        autohide: true,
                        delay: 5000,
                        title: 'Informasi',
                        body: '"Tidak dapat terhubung dengan server. Mohon coba lagi."'
                    })
                }
            });
        });
    });
</script>
@stop